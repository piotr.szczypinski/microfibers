Piotr M. Szczypinski,                            NIST, Gaithersburg 2016

MICROFIBERS PROJECT
-------------------

   Except as contained in this notice, the name of the NIST shall not be used
   in advertising or otherwise to promote the sale, use or other dealings
   in this Software without prior written authorization from the NIST.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
   OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
   IN THE SOFTWARE.


The microfibers project is a set of several programs intended for analysis of
confocal fluorescent microscopy images of cells growing on fiber or spun coat
scaffolds. The horvess program implements filtration algorithms to attenuate 
noise or enhance tubular-like structures of fiber scaffold. The threethres 
applies algorithms of image segmentation, to discriminate background, scaffold
and cell volumes. It also detects points of contact between scaffold and cells.
The animator use alpha-blending and rotation to produce animations of input 
images and contact points overlays. The scafgen produces artificial images of
cells and scaffold. These simulated images are intended for validation of 
various segmentation algorithms.

The source-codes were written during my stay with National Institute of 
Standards and Technology (NIST), Gaithersburg MD, USA, in 2016. Part of the 
code was adapted from my two other projects: qmazda and veselknife, both 
available from GitLab. All the programs intensively make use of InsightToolkit 
(ITK) library. The animator requires OpenCV and OpenGL.

The ./tutorial folder contains example images from confocal microscope, example 
script showing how to run the horvess, threethres and animator programs, and
setup files for scafgen simulator.

HORVES 

The horvess name is an abbreviation of horizontal vesselness, since among other 
algorithms it implements original algorithm for vesselness computation which 
enhances only tubular structures parallel to the XY plane. It processes 3D 
images, to enhance tube-like structures or to remove noise. It implements 
Gaussian kernel filtration and various vesselness computation algorithms. The 
multiscale approach for vesselness enhancement can be also used. The program 
requires ITK library and it utilizes getoption module from qmazda project.

Example: 
horvess -i input.nii -o output.nii -m Frangi -z -s 3 -l 0.4 -h 4.0

will apply modified Frangi vesselness enhancement to the input.nii image and 
will save results to the output.nii file. The multiscale approach is used with 
3 scales with Gaussian window of minimum standard deviation equal to 0.4 and 
maximum standard deviation equal to 4.0.

Calling the program with –help option will invoke a description and a list of 
available options:

Usage: horvess [OPTIONS]...
Computes vesselness of 3D data. Can suppress structures elongated in Z direction.
2016.08.08 by Piotr M. Szczypinski
Compilation date and time: Aug 29 2016 15:33:38
Options:
-m, --mode <mode>       Set <mode> to: 'Frangi', 'Sato', 'Erdt' or 'Gauss'
-z, --suppress-z        Suppress structures elongated in Z direction,
-s, --scales            Number of scales,
-l, --min_stdev         Minimum standard deviation (first scale) for filtration,
-h, --max_stdev         Maximum standard deviation (last scale) for filtration,
-i, --input <file>      Load 3D image from <file>,
-o, --output <file>     Save results to <file>,
/?, --help              Display this help and exit

THREETHRES 

The threethres gets the preprocessed images of a scaffold and a cell, performs 
voxel discrimination to background, cell and scaffold classes, and detects cell 
attachment points. The program requires (is dependent on) ITK library and it 
utilizes getoption module from qmazda project.

Example: 
threethres -c cell.nii -s scaf.nii -o out.nii -i 5000 -j 10000 –a 45 –n

will load cell image from cell.nii, scaffold image from scaf.nii, detect contact 
points and will save results to the out.nii file. The thresholds for 
cell-background and scaffold-background are defined with 5000 and 10000 
respectively, since the image in the example is coded by 16 bits per pixel 
unsigned values. It must be noted, that the thresholds must be defined 
accordingly to the gray-scale ranges of actual input images. If negative values 
are present in input images, the gray-scale can be shifted by means of –k and –l 
switches, in such a way that the black level is shifted to zero value. 

Calling the program with –help option will invoke a description and a list of 
available options as follows:

Usage: threethres [OPTIONS]...
For segmentation of two channel 3D image data.
2016.08.17 by Piotr M. Szczypinski
Compilation date and time: Aug 30 2016 09:30:31
Options:
-c, --cell <file>           Load 3D cell image from <file>,
-s, --scaffold <file>       Load 3D scaffold image from <file>,
-o, --output <file>         Save results to <file>,
-a, --cell-scaffold <f>     Cell-scaffold threshold of 0-90 degrees,
-i, --cell-backgrnd <f>     Cell-background gray-scale threshold,
-j, --scaffold-backgrnd <f> Scaffold-background gray-scale threshold,
-k, --cell-bias <f>         Cell bias,
-l, --scaffold-bias <f>     Scaffold bias,
-n, --attachments-only      Remove scaffold and cell markings,
-m, --min-max               Remove scaffold and cell markings,
/?, --help                  Display this help and exit.

ANIMATOR

Animator is a part of a project, which aims at analysis of confocal fluorescent 
microscopy images of cells growing on fiber scaffold. The animator presents 3D 
data in alpha-blending projection, in which red, green and blue colors represent
a cell image, a scaffold image and an overlay respectively. It rotates view of 
data using a specified rotation scheme and saves rendered images as video. The 
colors in the animator are mixed in accordance with RGB additive color model.

The rendering module of the animator is based on the VesselKnife project source 
code. Also, the program depends on OpenCV (video stream writer), OpenGL (3D 
rendering) and InsightToolkit (nifti format loader) libraries. Currently there 
are three versions of the animator to differently deal with OpenGL, based on 
glut, qt and wgl. The glut and qt versions were tested to work fine on OS X and 
Linux. The wgl version should finally work on Windows.

Example of use: 
animator -r scaf.nii -g cell.nii -b over.nii -v video.mov -f 250 -s 25 -m 1

It will load cell image from cell.nii, scaffold image from scaf.nii and overlay 
data from over.nii files. It will render projections in window (qt version) or 
into the invisible buffer (glut version), and save consecutive images into the 
video.mov file. The sequence consist of 250 frames at rate of 25 frames per 
second. The –m switch defines a rotation scheme, which in this case is a 360 
degrees rotation around Y axis. 

Calling the program with –help option will show a list of available options as 
follows:

Usage: animator [OPTIONS]...
Animates (rotates) 3D nifti images and saves video to a file.
2016.08.30 by Piotr M. Szczypinski
Compilation date and time: Sep 19 2016 10:59:41
Options:
  -w, --frame-width        positive integer number
  -h, --frame-height       positive integer number
  -r, --red-image          3D image for red channel
  -g, --green-image        3D image for green channel
  -b, --blue-image         3D image for blue channel
  -R, --red-weight         red weight for alpha channel
  -G, --green-weight       green weight for alpha channel
  -B, --blue-weight        blue weight for alpha channel
  -m, --rotation-mode      0, 1, ...6
  -v, --video-file         output video file name
  -f, --frames-to-save     positive integer number
  -s, --frames-per-second  positive floating point number
  -c, --fourcc-code        four-characters codec code, default is 'mp4v'
  -z, --zoom               scaling ratio, default is 400
  /?, --help

SCAFGEN

The scafgen program is a simulator/generator of artificial images that mimic 
real images from confocal fluorescent microscopy. It produces two channel (pairs
of) images which present scaffold and cells. Cells are simply modelled by 
ellipsoidal shapes. Scaffold can be eithed modelled as a fiber net, or a plate
-- a spun coat. Fibers are modelled by geometric blocks such as tubes or torus 
blocks.

The scafgen simulates several effects characteristic to 3D raster data and to 
the confocal fluorescent microscopy, such as: voxel partial volume effect, 
cross-talks (or cross-bleedings) between cell and scaffold images, optical 
distortions by means of kernel (point spread function) convolution and additive 
noise.

The program can be run from a console. It requires three arguments, the first
is an input file name of a text file with simulation parameters, the second is 
an output file name for cell image and the third is an output file name for 
scaffold image.

Example usage: scafgen setup.txt cell_output.nii scaffold_output.nii 

In ./tutorial folder there are examples of two setup files with comments 
explaining commands and syntax. 

The setup file may define a file name of the point spread function (PSF), used 
to model linear optical distortions. The file has to be available. The image 
should have odd dimensions in X, Y and Z directions, and store voxel values as 
floating point numbers. The example image of PSF is also available in the 
./tutorial folder.


