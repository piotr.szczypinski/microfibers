#include "management.h"

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>

Management::Management()
{
    animate = false;
    renderer = new ImageGl(frame_width, frame_height);
    red = new Byte3DImage();
    blue = new Byte3DImage();
    green = new Byte3DImage();
    alpha = new Byte3DImage();

    if(!loadAndSetImages())
    {
        printf("Could not load images or images are of different sizes.\n");
        fflush(stdout);
        return;
    }

    animate = true;
    renderer->setCrossView(3, false);
    renderer->setCrossView(1, false);
    renderer->setCrossView(2, false);
    renderer->setVolumeView(true);
    renderer->setZoom(double(-1000));

    buffer = (unsigned char*) malloc(4 * frame_width * frame_height);
    fflush(stdout);
}

void Management::rotationModel(unsigned int frame, double rotation[3])
{
    if(frame > frames_to_save)
    {
        switch(rotation_mode)
        {
            case 1:
            case 2:
            case 3:
            case 4:
                rotation[0] = 0; rotation[1] = 0; rotation[2] = 0; break;
            case 5: rotation[0] = 0; rotation[1] = 1.57079633; rotation[2] = 0; break;

            default: rotation[0] = 0; rotation[1] = 0; rotation[2] = -0.3; break;
        }
    }
    else
    {
        switch(rotation_mode)
        {

            case 1: rotation[0] = 0; rotation[1] = 0; rotation[2] = 6.28318531/frames_to_save; break;
            case 2: rotation[0] = 0; rotation[1] = 6.28318531/frames_to_save; rotation[2] = 0; break;
            case 3: rotation[0] = 0; rotation[1] = 4.44288294761/frames_to_save; rotation[2] = 4.44288294761/frames_to_save; break;
            case 4: rotation[0] = 0; rotation[1] = 4.44288294761/frames_to_save; rotation[2] = -4.44288294761/frames_to_save; break;
            case 5: rotation[0] = 0; rotation[1] = 0; rotation[2] = 6.28318531/frames_to_save; break;
            default:
            {
                double s = 2.5/frames_to_save * sin(6.28318531*frame/frames_to_save);
                double c = 2.5/frames_to_save * cos(6.28318531*frame/frames_to_save);

                rotation[0] = (double)0.52/frames_to_save;
                rotation[1] = c;
                rotation[2] = s;
                break;
            }
        }
    }
}

void Management::renderingLoop(void)
{
    double rotation[3];
    rotationModel(frames_to_save+1, rotation);
    renderer->rotateView(rotation[0], rotation[1], rotation[2]);
    renderer->paintGL();
    writer.open(video_file,
                           cv::VideoWriter::fourcc(fourcc_code[0],fourcc_code[1],fourcc_code[2],fourcc_code[3]),
                           frames_per_second, cv::Size(frame_width, frame_height), true);
    printf("Size: %i %i, Frames %i, Fps: %f, Fourcc: %c%c%c%c, Schema: %i\n", frame_width, frame_height,
           frames_to_save, frames_per_second,
           fourcc_code[0],fourcc_code[1],fourcc_code[2],fourcc_code[3], rotation_mode);

    for(int frame = 0; frame < frames_to_save; frame++)
    {
        renderer->ReadPixels(frame_width, frame_height, buffer);
        cv::Mat image = cv::Mat(cv::Size(frame_width, frame_height), CV_8UC3, buffer, cv::Mat::AUTO_STEP);
        cv::Mat fliped;
        if(writer.isOpened())
        {
            cv::flip(image, fliped, 0);
            writer.write(fliped);
            printf("Rendering %i of %i\n", frame+1, frames_to_save); fflush(stdout);
        }
        rotationModel(frame, rotation);
        renderer->rotateView(rotation[0], rotation[1], rotation[2]);
        renderer->paintGL();
    }
    writer.release();
}

bool Management::loadAndSetImages(void)
{
    if(!loadNifti(red_image, red)) return false;
    if(!loadNifti(green_image, green)) return false;
    if(!loadNifti(blue_image, blue)) return false;

    float diagonal = 0;
    for(int d = 0; d < 3; d++)
    {
        if(red->size[d] != green->size[d] || red->size[d] != blue->size[d]) return false;
        diagonal += (red->size[d]*red->voxelsize[d]*red->size[d]*red->voxelsize[d]);
    }
    diagonal = sqrt(diagonal);
    diagonal = rescaling/diagonal;

    if(!generateAlpha()) return false;
    for(int d = 0; d < 3; d++)
    {
        alpha->voxelsize[d] = blue->voxelsize[d] = green->voxelsize[d] = red->voxelsize[d] = red->voxelsize[d]*diagonal;
    }

    renderer->setImage(0, red);
    renderer->setImage(1, green);
    renderer->setImage(2, blue);
    renderer->setImage(3, alpha);
    return true;
}

bool Management::generateAlpha(void)
{
    unsigned int d;

    if(red == NULL || blue == NULL || green == NULL) return false;

    alpha = new Byte3DImage();
    alpha->allocate(green->size);
    for(d = 0; d < 3; d++)
        alpha->voxelsize[d] = green->voxelsize[d];

    unsigned int max = alpha->allsize();
    unsigned char* ptrr = red->data;
    unsigned char* ptrg = green->data;
    unsigned char* ptrb = blue->data;
    unsigned char* ptro = alpha->data;
    for(d = 0; d < max; d++)
    {
        float v = weightRed**ptrr + weightGreen**ptrg + weightBlue**ptrb + bias;
        if(v > 255) v = 255;
        if(v < 0) v = 0;
        *ptro = v;
        ptro++; ptrr++; ptrg++; ptrb++;
    }
    return true;
}


bool Management::loadNifti(const char* filename, Byte3DImage* image, unsigned int frame)
{
    unsigned int d;
    if(image != NULL)
    {

        nifti_image* nim = NULL;
        nim = nifti_image_read(filename, 1);
        if(nim == NULL) return false;
        unsigned int sizemore = 1;
        for(d = 3; d < nim->dim[0]; d++) sizemore *= nim->dim[d+1];

        if(frame >= sizemore)
        {
            nifti_image_free(nim);
            return false;   //Failed
        }

        unsigned int size[BI_DIMENSIONS];
        for(d = 0; d < nim->dim[0] && d < BI_DIMENSIONS; d++) size[d] = nim->dim[d+1];
        for(; d < BI_DIMENSIONS; d++) size[d] = 1;
        unsigned int sizeall = 1;
        for(d = 0; d < BI_DIMENSIONS; d++) sizeall *= size[d];
        unsigned int linesnumber = 1;
        for(d = 1; d < BI_DIMENSIONS; d++) linesnumber *= size[d];

        switch(nim->datatype)
        {
        case DT_UINT8:
        case DT_INT16:
        case DT_UINT16:
        case DT_FLOAT32:
        case DT_FLOAT64:
        case DT_COMPLEX64:
            image->allocate(size);
            break;

        default:
            nifti_image_free(nim);
            return false;   //Failed
        }

        for(d = 0; d < nim->dim[0] && d < BI_DIMENSIONS; d++) image->voxelsize[d] = nim->pixdim[d+1];

        switch(nim->datatype)
        {
        case DT_UINT8:
            {
                uint8 *dptr;
                uint8 *ptr;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((uint8*)nim->data) + frame*sizeall + (size[0] * d);
                    memcpy(dptr, ptr, size[0]);
                }
            }
            break;

        case DT_INT16:
        {
            uint8 *dptr;
            int16 *ptr = (int16*)nim->data + frame*sizeall;
            double min = *ptr;
            double max = *ptr;
            for(d = 0; d < linesnumber; d++)
            {
                ptr = ((int16*)nim->data) + frame*sizeall + (size[0] * d);
                for(unsigned int x = 0; x < size[0]; x++)
                {
                    if(min > *ptr) min = *ptr;
                    if(max < *ptr) max = *ptr;
                    ptr++;
                }
            }
            if(max > min)
            {
                max -= min;
                max /= 255.9;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((int16*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        *dptr = (*ptr-min)/max;
                        ptr++; dptr++;
                    }
                }
            }
        }
        break;
        case DT_UINT16:
        {
            uint8 *dptr;
            uint16 *ptr = (uint16*)nim->data + frame*sizeall;
            double min = *ptr;
            double max = *ptr;
            for(d = 0; d < linesnumber; d++)
            {
                ptr = ((uint16*)nim->data) + frame*sizeall + (size[0] * d);
                for(unsigned int x = 0; x < size[0]; x++)
                {
                    if(min > *ptr) min = *ptr;
                    if(max < *ptr) max = *ptr;
                    ptr++;
                }
            }
            if(max > min)
            {
                max -= min;
                max /= 255.9;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((uint16*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        *dptr = (*ptr-min)/max;
                        ptr++; dptr++;
                    }
                }
            }
        }
        break;
        case DT_FLOAT32:
        {
            uint8 *dptr;
            float *ptr = (float*)nim->data + frame*sizeall;
            float min = *ptr;
            float max = *ptr;
            for(d = 0; d < linesnumber; d++)
            {
                ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
                for(unsigned int x = 0; x < size[0]; x++)
                {
                    if(min > *ptr) min = *ptr;
                    if(max < *ptr) max = *ptr;
                    ptr++;
                }
            }
            if(max > min)
            {
                max -= min;
                max /= 255.9;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        *dptr = (*ptr-min)/max;
                        ptr++; dptr++;
                    }
                }
            }
        }
        break;
        case DT_FLOAT64:
            {
                uint8 *dptr;
                double *ptr = (double*)nim->data + frame*sizeall;
                double min = *ptr;
                double max = *ptr;
                for(d = 0; d < linesnumber; d++)
                {
                    ptr = ((double*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        if(min > *ptr) min = *ptr;
                        if(max < *ptr) max = *ptr;
                        ptr++;
                    }
                }
                if(max > min)
                {
                    max -= min;
                    max /= 255.9;
                    for(d = 0; d < linesnumber; d++)
                    {
                        dptr = ((uint8*)image->data) + (image->size[0] * d);
                        ptr = ((double*)nim->data) + frame*sizeall + (size[0] * d);
                        for(unsigned int x = 0; x < size[0]; x++)
                        {
                            *dptr = (*ptr-min)/max;
                            ptr++; dptr++;
                        }
                    }
                }
            }
            break;

        case DT_COMPLEX64:
        {
            uint8 *dptr;
            float *ptr = (float*)nim->data + frame*sizeall;
            float min = *ptr;
            float max = *ptr;
            for(d = 0; d < linesnumber; d++)
            {
                ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
                for(unsigned int x = 0; x < size[0]; x++)
                {
                    float re = *ptr; ptr++;
                    float im = *ptr; ptr++;
                    float mag = sqrt(re*re + im*im);
                    if(min > mag) min = mag;
                    if(max < mag) max = mag;
                }
            }
            if(max > min)
            {
                max -= min;
                max /= 255.9;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        float re = *ptr; ptr++;
                        float im = *ptr; ptr++;
                        float mag = sqrt(re*re + im*im);
                        *dptr = (mag-min)/max; dptr++;
                    }
                }
            }
        }
        break;
        case DT_RGB24:
            {
                uint8 *dptr;
                uint8 *ptr = (uint8*)nim->data + frame*sizeall;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((uint8*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        *dptr = *ptr; dptr++;
                        *dptr = *ptr; dptr++;
                        *dptr = *ptr; dptr++;
                    }
                }
            }
            break;

        default:
            nifti_image_free(nim);
            return false;   //Failed
        }

        std::stringstream ss;
        ss << "File: " << filename << std::endl;

        ss << "Format: ";
        switch(nim->nifti_type)
        {
        case 0: ss << "Analyze"; break;
        case 1: ss << "Nifti-1 (single file)"; break;
        case 2: ss << "Nifti-1 (two files)"; break;
        case 3: ss << "Nifti-ASCII"; break;
        default: ss << "?";
        }
        ss << std::endl;

        ss << "Raster: ";
        for(d = 1; (int)d <= nim->dim[0]; d++)
        {
            ss << nim->dim[d];
            if((int)d < nim->dim[0]) ss << "x";
        }
        ss << std::endl;
        if(sizemore > 1)
        {
            ss << "Frame: ";
            ss << frame << " of " << sizemore;
            ss << std::endl;
        }

        ss << "Original voxel type: ";
        switch(nim->datatype)
        {
        case DT_UINT8: ss << "uint8"; break;
        case DT_INT16: ss << "int16"; break;
        case DT_UINT16: ss << "uint16"; break;
        case DT_FLOAT32: ss << "float32"; break;
        case DT_FLOAT64: ss << "float64"; break;
        case DT_COMPLEX64: ss << "complex64"; break;
        case DT_RGB24:  ss << "rgb24"; break;
        default: ss << "?"; break;
        }
        ss << std::endl;
        ss << "Stored voxel type: uint8";
        ss << std::endl;

        ss << "Voxel size: ";
        for(d = 1; (int)d <= nim->dim[0]; d++)
        {
            ss << nim->pixdim[d];
            if((int)d < nim->dim[0]) ss << "x";
        }
        ss << std::endl;

        ss << "Space units: ";
        switch(nim->xyz_units)
        {
        case NIFTI_UNITS_METER: ss << "m"; break;
        case NIFTI_UNITS_MM: ss << "mm"; break;
        case NIFTI_UNITS_MICRON: ss << "um"; break;
        default: ss << "?";
        }
        ss << std::endl;
        ss << "Time units: ";
        switch(nim->time_units)
        {
        case NIFTI_UNITS_SEC: ss << "s"; break;
        case NIFTI_UNITS_MSEC: ss << "ms"; break;
        case NIFTI_UNITS_USEC: ss << "us"; break;
        default: ss << "?";
        }
        ss << std::endl;

        ss << "Slope: " << nim->scl_slope << std::endl;
        ss << "Intercept: " << nim->scl_inter << std::endl;
        ss << "Calibration minimum : " << nim->cal_min << std::endl;
        ss << "Calibration maximum: " << nim->cal_max << std::endl;

        if(image->info != NULL) delete[] image->info;
        image->info = new char[ss.str().length()+2];
        strcpy(image->info, ss.str().c_str());

        if(image->filePathAndName != NULL) delete[] image->filePathAndName;
        image->filePathAndName = new char[strlen(filename)+2];
        strcpy(image->filePathAndName, filename);
        image->framesInFile = sizemore;
        image->currentFrame = frame;


        nifti_image_free(nim);
        return true;
    }

    return false;
}
