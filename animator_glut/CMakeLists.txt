cmake_minimum_required(VERSION 2.8)

project(animator)
add_executable(animator MACOSX_BUNDLE animator.cpp management.cpp imagegl.cpp)

find_package(ITK REQUIRED)
include(${ITK_USE_FILE})
if( "${ITK_VERSION_MAJOR}" LESS 4 )
  target_link_libraries(animator ITKReview ${ITK_LIBRARIES})
else( "${ITK_VERSION_MAJOR}" LESS 4 )
  target_link_libraries(animator ${ITK_LIBRARIES})
endif( "${ITK_VERSION_MAJOR}" LESS 4 )

find_package(OpenCV REQUIRED)
target_link_libraries(animator ${OpenCV_LIBS} )

find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
include_directories( ${OPENGL_INCLUDE_DIRS}  ${GLUT_INCLUDE_DIRS} )
target_link_libraries(animator ${OPENGL_LIBRARIES} ${GLUT_LIBRARY} )
