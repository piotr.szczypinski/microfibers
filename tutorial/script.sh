#! /bin/sh
# This script presents how to use programs from this project to process 
# images of fiber scaffold and cell, and to produce video with a 3D animation

# Filter fiber scaffold image to enhance fibers. Modified Frangi's vesselness is used.
./horvess -m Frangi -z -s 1 -l 1.0 -h 1.0 -i scaf.nii -o scaf_F10.nii

# Filter cell image to remove noise. Gaussian kernel convolution is used.
./horvess -m Gauss -s 1 -l 0.3 -h 0.3 -i cell.nii -o cell_G03.nii

# Segment two images and find attachment areas between cell and fiber scaffold
threethres -c cell_G03.nii -s scaf_F10.nii -o cont.nii -i 5000 -j 10000 –a 45 –n
# If does not work, verify ranges of gray-scales with -m 
# and set threshold values accordingly 

# Animate 3D data and save video to a file
./animator -r cell.nii -g scaf.nii -b cont.nii -m 1 -v animation.mov -c mp4v -f 128 -s 24

# Optionally, if the above deos not work:
# Animate 3D data and save separate frames to the subfolder
./animator -r cell.nii -g scaf.nii -b cont.nii -v ./Frames/frame%04d.bmp -f 128 -s 0
# Use ffmpeg (see: www.ffmpeg.org) to create video from the frames
ffmpeg -i ./Frames/frame%04d.bmp -c:v libx264 -preset slow -crf 22 -pix_fmt yuv420p animation.mp4
