/*
 * scafgen
 * Optical microscopy image generator. Produces 3D image data similar
 * to confocal microscope images of fiber scaffolds and bacteria. Intended
 * for validation and testing of image analysis procedures.
 * Requires Insight Toolkit (ITK) version 4.20 or higher.
 *
 * Copyright (c) 2016 by Piotr M. Szczypiński
 * and NIST (National Institute of Standards and Technology)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of the NIST shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization from the NIST.
 */

#include <string>
#include <sstream>


#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>

#include <itkAdditiveGaussianNoiseImageFilter.h>
#include <itkShotNoiseImageFilter.h>

#include <itkConvolutionImageFilter.h>
#include <itkFFTConvolutionImageFilter.h>


typedef itk::Image<float, 3>  ImageType;

struct Torus
{
    double R; //major radius
    double r; //tube radius
    double x0; //center coordinates
    double y0;
    double z0;
    double ax; //rotation angles
    double ay;
};
std::vector< Torus > torus_parameters;

struct Tube
{
    double r; //tube radius
    double x0; //point
    double y0;
    double z0;
    double x1; //point
    double y1;
    double z1;
};
std::vector< Tube > tube_parameters;

struct Plate
{
    double x; //normal vector
    double y;
    double z;
    double d; //distance from the center of coordinate system
    double t; //thickness
};
std::vector< Plate > plate_parameters;

struct Elipsoid
{
    double rx; //radii
    double ry;
    double rz;
    double x0; //center coordinates
    double y0;
    double z0;
    double ax; //rotation angles
    double ay;
    double az;
};
std::vector< Elipsoid > cell_parameters;

std::string kernel_name;

//=============================================================================
// Parameters: it would be better to load the below parameters from a text file

// Grey levels are in range 0.0 to max_grey_level
double min_grey_level = 0.0;
double max_grey_level = 1.0;

// Image size in voxels
unsigned int size[3] = {128, 128, 32};

// Voxel subsampling for partial volume effect modeling
unsigned int subsize[3] = {4, 4, 8};

// Voxel size in some metric units (mm)
double spacing[3] = {0.120, 0.120, 0.462};

// Crosstalk (cross-bleeding) amount
double crosstalk_from_cell = 0.0;
double crosstalk_from_scaf = 0.0;

// Apply convolution filter to model optical distortion
bool apply_convolution_filter = false;

// Gaussian, additive noise amount
double cell_noise_stddev = 0.0;
double scaf_noise_stddev = 0.0;

// Poisson, scale factor
double cell_noise_scale = 0.0;
double scaf_noise_scale = 0.0;

// Set it to true to skip all the distortions and noise modeling
bool ground_truth = false;

//=============================================================================
// x,y,z rotation around X, Y and Z axes
void Rotate(double* x, double* y, double* z, double ax, double ay, double az)
{
    double x1, y1, z1;
    double cosa, sina;

    if(ax != 0.0)
    {
        cosa = cos(ax);
        sina = sin(ax);
        y1 = *y * cosa - *z *sina;
        z1 = *y * sina + *z *cosa;
        *y = y1;
        *z = z1;
    }
    if(ay != 0.0)
    {
        cosa = cos(ay);
        sina = sin(ay);
        x1 = *x * cosa - *z *sina;
        z1 = *x * sina + *z *cosa;
        *x = x1;
        *z = z1;
    }
    if(az != 0.0)
    {
        cosa = cos(az);
        sina = sin(az);
        x1 = *x * cosa - *y *sina;
        y1 = *x * sina + *y *cosa;
        *x = x1;
        *y = y1;
    }
}

//=============================================================================
// Is the x,y,z point within the n-th ellipsoid?
bool IsCell(double x, double y, double z, unsigned int n)
{
    x -= cell_parameters[n].x0;
    y -= cell_parameters[n].y0;
    z -= cell_parameters[n].z0;

    Rotate(&x, &y, &z, cell_parameters[n].ax, cell_parameters[n].ay, cell_parameters[n].az);

    x /= cell_parameters[n].rx;
    y /= cell_parameters[n].ry;
    z /= cell_parameters[n].rz;

    if(x*x + y*y + z*z < 1.0) return true;
    return false;
}

//=============================================================================
// Is the x,y,z point within the n-th plate?
bool IsSpuncoat(double x, double y, double z, unsigned int n)
{
    double thickhalf = plate_parameters[n].t/2;
    double dist = plate_parameters[n].x * x + plate_parameters[n].y * y + plate_parameters[n].z * z - plate_parameters[n].d;
    if(dist >= -thickhalf && dist <= thickhalf)
        return true;
    return false;
}

//=============================================================================
// Is the x,y,z point within the n-th torus?
bool IsTorusFiber(double x, double y, double z, unsigned int n)
{
    x -= torus_parameters[n].x0;
    y -= torus_parameters[n].y0;
    z -= torus_parameters[n].z0;

    Rotate(&x, &y, &z, torus_parameters[n].ax, torus_parameters[n].ay, 0.0);

    double R = torus_parameters[n].R;
    double r = torus_parameters[n].r;

    double a1 = R - sqrt(x*x + y*y);
    return (a1*a1+z*z < r*r);
}

//=============================================================================
// Is the x,y,z point within the n-th tube?
bool IsTubeFiber(double x, double y, double z, unsigned int n)
{
    static double denom;
    static double zv, yv, xv;
    static unsigned int last_index = (unsigned int)-1;
    double zu, yu, xu;
    double nom1, nom2, nom3;
    double dists;

    if(last_index != n)
    {
        last_index = n;
        zv = tube_parameters[n].z1 - tube_parameters[n].z0;
        yv = tube_parameters[n].y1 - tube_parameters[n].y0;
        xv = tube_parameters[n].x1 - tube_parameters[n].x0;
        denom = (zv*zv + yv*yv +xv*xv);
    }

    zu = tube_parameters[n].z0 - z;
    yu = tube_parameters[n].y0 - y;
    xu = tube_parameters[n].x0 - x;

    nom1 = (yu*zv - zu*yv);
    nom2 = (zu*xv - xu*zv);
    nom3 = (xu*yv - yu*xv);

    dists = (nom1*nom1+nom2*nom2+nom3*nom3)/denom;
    if(dists < tube_parameters[n].r*tube_parameters[n].r) return true;
    return false;
}

//=============================================================================
// Is the x,y,z point within any of the defined ellipsoids?
bool IsCell(double x, double y, double z)
{
    unsigned int i;
    unsigned int cell_count = cell_parameters.size();

    for(i = 0; i < cell_count; i++)
    {
        if(IsCell(x, y, z, i)) return true;
    }
    return false;
}

//=============================================================================
// Is the x,y,z point within any of the defined toruses or tubes?
bool IsScaffold(double x, double y, double z)
{
    unsigned int i;
    unsigned int tube_count = tube_parameters.size();
    unsigned int torus_count = torus_parameters.size();
    unsigned int plate_count = plate_parameters.size();

    for(i = 0; i < torus_count; i++)
    {
        if(IsTorusFiber(x, y, z, i)) return true;
    }
    for(i = 0; i < tube_count; i++)
    {
        if(IsTubeFiber(x, y, z, i)) return true;
    }
    for(i = 0; i < plate_count; i++)
    {
        if(IsSpuncoat( x,  y,  z, i)) return true;
    }
    return false;
}

//=============================================================================
// Alocates image buffers
void CreateEmptyImage(ImageType::Pointer image, unsigned int size[3], double spac[3])
{
    ImageType::RegionType Region;
    ImageType::RegionType::IndexType Start;
    ImageType::RegionType::SizeType Size;

    double orgi[3];
    for(int i = 0; i < 3; i++)
    {
        Start[i] = 0;
        Size[i]  = size[i];
        orgi[i] = 0;
    }
    Region.SetIndex( Start );
    Region.SetSize( Size );

    image->SetRegions( Region );
    image->SetOrigin(orgi);
    image->SetSpacing(spac);
    image->Allocate();
    image->FillBuffer(0.0);
}

//=============================================================================
// Saves image to the file of fileName
void WriteImage(const ImageType::Pointer image, const std::string& fileName)
{
    typedef  itk::ImageFileWriter< ImageType  > WriterType;
    WriterType::Pointer writer = WriterType::New();
    writer->SetFileName(fileName);
    writer->SetInput(image);
    writer->Update();
}

//=============================================================================
// Creates ideal images of scuffold and cells - geometry rasterization
void FillInImages(ImageType::Pointer cell_image, ImageType::Pointer scaf_image)
{
    unsigned int greylevels = subsize[2];
    for(int i = 0; i < 2; i++) greylevels *= subsize[i];

    ImageType::IndexType ixyz;

    for(ixyz[2] = 0; ixyz[2] < size[2]; ixyz[2]++)
    {
        for(ixyz[1] = 0; ixyz[1] < size[1]; ixyz[1]++)
            for(ixyz[0] = 0; ixyz[0] < size[0]; ixyz[0]++)
            {
                unsigned int fvoxel = 0;
                unsigned int cvoxel = 0;

                for(unsigned int dz = 0; dz < subsize[2]; dz++)
                    for(unsigned int dy = 0; dy < subsize[1]; dy++)
                        for(unsigned int dx = 0; dx < subsize[0]; dx++)
                        {
                            double z, y, x;

                            // 0,0,0 point in a center of the image
                            z = (double)ixyz[2] - (double)size[2]/2.0;
                            y = (double)ixyz[1] - (double)size[1]/2.0;
                            x = (double)ixyz[0] - (double)size[0]/2.0;

                            // Partial volume effect
                            z = (z + ((double)dz+0.5)/subsize[2])*spacing[2];
                            y = (y + ((double)dy+0.5)/subsize[1])*spacing[1];
                            x = (x + ((double)dx+0.5)/subsize[0])*spacing[0];

                            bool is_fiber = IsScaffold(x, y, z);
                            bool is_cell = IsCell(x, y, z);

                            if(is_fiber) fvoxel++;
                            if(is_cell & (!is_fiber)) cvoxel++;
                        }
                scaf_image->SetPixel(ixyz, (max_grey_level-min_grey_level)*((double)fvoxel/greylevels+min_grey_level));
                cell_image->SetPixel(ixyz, (max_grey_level-min_grey_level)*((double)cvoxel/greylevels+min_grey_level));
            }
        printf("Generating image cross-section %i of %i\n", (int)(ixyz[2])+1, size[2]);
        fflush(stdout);
    }
}

//=============================================================================
// Crosstalks simulation
void CrossTalks(ImageType::Pointer cell_image, ImageType::Pointer scaf_image)
{
    ImageType::IndexType ixyz;

    for(ixyz[2] = 0; ixyz[2] < size[2]; ixyz[2]++)
    {
        for(ixyz[1] = 0; ixyz[1] < size[1]; ixyz[1]++)
            for(ixyz[0] = 0; ixyz[0] < size[0]; ixyz[0]++)
            {
                double cell = cell_image->GetPixel(ixyz);
                double scaf = scaf_image->GetPixel(ixyz);

                double nscaf = cell*crosstalk_from_cell + scaf*(1.0 - crosstalk_from_cell);
                double ncell = scaf*crosstalk_from_scaf + cell*(1.0 - crosstalk_from_scaf);

                scaf_image->SetPixel(ixyz, nscaf);
                cell_image->SetPixel(ixyz, ncell);
            }
        printf("Cross talks cross-section %i of %i\n", (int)(ixyz[2])+1, size[2]);
        fflush(stdout);
    }
}

//=============================================================================
// Adding Gaussian noise to the image
void GaussianNoise(ImageType::Pointer* image, double sigma)
{
    typedef itk::AdditiveGaussianNoiseImageFilter<ImageType, ImageType> GaussianNoiseFilterType;
    GaussianNoiseFilterType::Pointer gaussian_noise_filter = GaussianNoiseFilterType::New();
    gaussian_noise_filter->SetInput(*image);
    gaussian_noise_filter->SetMean(0.0);
    gaussian_noise_filter->SetStandardDeviation(sigma);
    gaussian_noise_filter->Update();
    
    *image = gaussian_noise_filter->GetOutput();
}

//=============================================================================
// Adding Poisson noise to the image
void PoissonNoise(ImageType::Pointer* image, double scale)
{
    typedef itk::ShotNoiseImageFilter<ImageType, ImageType> ShotNoiseFilterType;
    ShotNoiseFilterType::Pointer shot_noise_filter = ShotNoiseFilterType::New();
    shot_noise_filter->SetInput(*image);
    shot_noise_filter->SetScale(scale);
//    gaussian_noise_filter->SetStandardDeviation(sigma);
    shot_noise_filter->Update();
    
    *image = shot_noise_filter->GetOutput();
}
//=============================================================================
// Computes image and kernel convolution for optical distortion modeling
void OpticalDistortions(ImageType::Pointer* image, ImageType::Pointer kernel)
{
    //typedef itk::ConvolutionImageFilter<ImageType> FilterType;
    typedef itk::FFTConvolutionImageFilter<ImageType> FilterType;
    FilterType::Pointer convolutionFilter = FilterType::New();
    convolutionFilter->SetInput(*image);
    convolutionFilter->SetKernelImage(kernel);
    convolutionFilter->Update();
    
    *image = convolutionFilter->GetOutput();
}

//=============================================================================
// Loads parameters from the setup file
bool LoadSetup(const char *fileName)
{
    std::ifstream file;
    file.open(fileName);
    if ((!file.is_open()) || (!file.good()))
    {
        printf("Cannot open file.\n");
        return false;
    }
    std::string line;
    int line_number = 0;
    while (std::getline(file, line))
    {
        line_number ++;
        std::string token;
        std::stringstream sline(line);
        sline >> token;
        bool parsed = false;
        if(token == "Cell")
        {
            Elipsoid o;
            sline >> o.rx >> o.ry >> o.rz >> o.x0 >> o.y0 >> o.z0 >> o.ax >> o.ay >> o.az;
            cell_parameters.push_back(o);
            parsed = true;
        }
        else if(token == "Torus")
        {
            Torus o;
            sline >> o.R >> o.r >> o.x0 >> o.y0 >> o.z0 >> o.ax >> o.ay;
            torus_parameters.push_back(o);
            parsed = true;
        }
        else if(token == "Tube")
        {
            Tube o;
            sline >> o.r >> o.x0 >> o.y0 >> o.z0 >> o.x1 >> o.y1 >> o.z1;
            tube_parameters.push_back(o);
            parsed = true;
        }
        else if(token == "Plate")
        {
            Plate o;
            sline >> o.x >> o.y >> o.z >> o.d >> o.t;
            plate_parameters.push_back(o);
            parsed = true;
        }
        else if(token == "DistortionKernelFile")
        {
            sline >> kernel_name;
            parsed = true;
        }
        else if(token == "GreyLevels")
        {
            sline >> min_grey_level >> max_grey_level;
            parsed = true;
        }
        else if(token == "ImageSize")
        {
            sline >> size[0] >> size[1] >> size[2];
            parsed = true;
        }
        else if(token == "PartialVolumeDividers")
        {
            sline >> subsize[0] >> subsize[1] >> subsize[2];
            parsed = true;
        }
        else if(token == "VoxelSpacing")
        {
            sline >> spacing[0] >> spacing[1] >> spacing[2];
            parsed = true;
        }
        else if(token == "CrossTalks")
        {
            sline >> crosstalk_from_cell >> crosstalk_from_scaf;
            parsed = true;
        }
        else if(token == "GaussianNoise")
        {
            sline >> cell_noise_stddev >> scaf_noise_stddev;
            parsed = true;
        }
        else if(token == "ShotNoise")
        {
            sline >> cell_noise_scale >> scaf_noise_scale;
            parsed = true;
        }

        else if(token == "GroundTruth")
        {
            ground_truth = true;
            parsed = true;
        }



        if(parsed && sline.fail())
        {
            printf("Error in line %i\n", line_number);
            return false;
        }
        else if(token[0] == '#' or token[0] == '/')
        {
            // This was comment
        }
        else if(! parsed)
        {
            printf("Warning: line %i is unrecognized or empty\n", line_number);
        }
    }
    file.close();
    if(line_number == 0)
    {
        printf("Cannot read file.\n");
        return false;
    }

    return true;
}

//=============================================================================
// Program starts in main()
int main(int argc, char *argv[])
{
    if(argc < 3)
    {
        printf("Arguments are missing.\nExample: %s setup.txt cell.nii scaffold.nii\n", argv[1]);
        return 0;
    }
    if(! LoadSetup(argv[1]))
    {
        printf("Cannot read setup parameters from: %s\n", argv[1]);
        return -1;
    }
    ImageType::Pointer optical_distortions;
    ImageType::Pointer scaf_image = ImageType::New();
    ImageType::Pointer cell_image = ImageType::New();

    typedef itk::ImageFileReader< ImageType  > ReaderType;
    ReaderType::Pointer reader = ReaderType::New();
    if(! kernel_name.empty())
    {
        reader->SetFileName(kernel_name);
        try
        {
            reader->Update();
            optical_distortions = reader->GetOutput();
            apply_convolution_filter = true;
            printf("Distortions kernel loaded from %s\n", kernel_name.c_str()); fflush(stdout);
        }
        catch (itk::ExceptionObject &ex)
        {
            printf("Error loading distortions kernel from %s\n", kernel_name.c_str()); fflush(stdout);
            std::cout << ex << std::endl;
            return -2;
        }
    }

    CreateEmptyImage(scaf_image, size, spacing);
    CreateEmptyImage(cell_image, size, spacing);
    printf("Image buffers allocated\n"); fflush(stdout);

    FillInImages(cell_image, scaf_image);
    printf("Rasterization done\n"); fflush(stdout);

    if(ground_truth)
    {
        WriteImage(cell_image, argv[2]);
        WriteImage(scaf_image, argv[3]);
        printf("Undistorted data saved to %s and %s files.\n", argv[2], argv[3]);
        return 0;
    }

    CrossTalks(cell_image, scaf_image);

    if(apply_convolution_filter)
    {
        printf("Starts distortions - wait patiently.\n"); fflush(stdout);
        OpticalDistortions(&cell_image, optical_distortions);
        printf("Cell distortions done\n"); fflush(stdout);
        OpticalDistortions(&scaf_image, optical_distortions);
        printf("Scaffold distortions done\n"); fflush(stdout);
    }

    if(cell_noise_stddev > 0.0)
    {
        GaussianNoise(&cell_image, cell_noise_stddev);
        printf("Gaussian noise added to cell\n"); fflush(stdout);
    }
    if(scaf_noise_stddev > 0.0)
    {
        GaussianNoise(&scaf_image, scaf_noise_stddev);
        printf("Gaussian noise added to scaffold\n"); fflush(stdout);
    }

    if(cell_noise_scale > 0.0)
    {
        PoissonNoise(&cell_image, cell_noise_scale);
        printf("Shot noise added to cell\n"); fflush(stdout);
    }
    if(scaf_noise_scale  > 0.0)
    {
        PoissonNoise(&scaf_image, scaf_noise_scale);
        printf("Shot noise added to scaffold\n"); fflush(stdout);
    }

    WriteImage(cell_image, argv[2]);
    WriteImage(scaf_image, argv[3]);
    printf("Data saved to %s and %s files.\n", argv[2], argv[3]);

    return 0;
}
