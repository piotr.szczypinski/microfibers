/*=========================================================================
 *
 * This file was modified by Piotr M. Szczypinski in 2016.08 to:
 *  1. Enable computation of Frangi's and Erdt's vesselness
 *  2. Implement procedure for suppression of structures elongated in Z axis
 *  3. Optimize memory management
 *  4. Implement multithreading computation
 *
 * This code is based on ITK's Hessian3DToVesselnessMeasureImageFilter.
 *
 *=========================================================================*/

/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
#ifndef itkScaffoldHessianToVesselnessFilter_hxx
#define itkScaffoldHessianToVesselnessFilter_hxx

#include "itkScaffoldHessianToVesselnessFilter.h"
#include "itkImageRegionIterator.h"
#include "itkMath.h"

namespace itk
{
/**
 * Constructor
 */
template< typename TPixel >
ScaffoldHessianToVesselnessFilter< TPixel >
::ScaffoldHessianToVesselnessFilter()
{
    m_Method = Sato;
    m_Param1 = 0.5;
    m_Param2 = 2.0;

    // Hessian( Image ) = Jacobian( Gradient ( Image ) )  is symmetric
    m_SymmetricEigenValueFilter = EigenAnalysisFilterType::New();
    m_SymmetricEigenValueFilter->SetDimension(ImageDimension);
    m_SymmetricEigenValueFilter->OrderEigenValuesBy(
                EigenAnalysisFilterType::FunctorType::OrderByValue);
}

/*
Implementation of Sato algorithm according to previous ITK implementation
    itkHessian3DToVesselnessMeasureImageFilter.hxx

    Sato, Yoshinobu, et al. "3D multi-scale line filter for segmentation and
    visualization of curvilinear structures in medical images."
    CVRMed-MRCAS'97. Springer Berlin Heidelberg, 1997.

Implementation of Frangi and Erdt algorithms according to:
    Frangi, Alejandro F., et al. "Multiscale vessel enhancement filtering."
    International Conference on Medical Image Computing and Computer-Assisted Intervention.
    Springer Berlin Heidelberg, 1998.

    Drechsler, Klaus, and Cristina Oyarzun Laura.
    "Comparison of vesselness functions for multiscale analysis of the liver vasculature."
    Proceedings of the 10th IEEE International Conference on Information Technology
    and Applications in Biomedicine. IEEE, 2010.

//Implementation of Hannink algorithm according to:
//    Hannink, Julius, Remco Duits, and Erik Bekkers. "Crossing-preserving multi-scale vesselness."
//    International Conference on Medical Image Computing and Computer-Assisted Intervention.
//    Springer International Publishing, 2014.
// Not implemented, 2D only

Implementation of XYPlane algorithm original.
*/


template< typename TPixel >
void
ScaffoldHessianToVesselnessFilter< TPixel >
::BeforeThreadedGenerateData(void)
{
    this->AllocateOutputs();
}

template< typename TPixel >
void
ScaffoldHessianToVesselnessFilter< TPixel >
::ThreadedGenerateData(const OutputImageRegionType & region, ThreadIdType threadId)
{
    itkDebugMacro(<< "ScaffoldHessianToVesselnessFilter generating data, Thread " << threadId);

    typename InputImageType::ConstPointer hessian_input = this->GetInput();
    typename OutputImageType::Pointer vesselness_output = this->GetOutput();
    ImageRegionConstIterator< InputImageType > iter_hessian_input;
    iter_hessian_input = ImageRegionConstIterator< InputImageType >(hessian_input, region);
    ImageRegionIterator< OutputImageType > iter_vesselness_output;
    iter_vesselness_output = ImageRegionIterator< OutputImageType >(vesselness_output, region);

    typedef itk::FixedArray< double, ImageDimension > EigenValuesArrayType;
    typedef itk::Matrix< double, ImageDimension, ImageDimension > EigenVectorMatrixType;
    typedef itk::SymmetricEigenAnalysis< InputPixelType, EigenValuesArrayType, EigenVectorMatrixType > SymmetricEigenAnalysisType;

    EigenValuesArrayType eigenValue;
    EigenVectorMatrixType eigenVector;
    SymmetricEigenAnalysisType symmetricEigenSystem(ImageDimension);

    iter_hessian_input.GoToBegin();
    iter_vesselness_output.GoToBegin();

    switch(m_Method)
    {

    case Sato:
        symmetricEigenSystem.SetOrderEigenValues(true);
        while ( !iter_hessian_input.IsAtEnd() )
        {
            symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
            double normalizeValue = std::min(-1.0 * eigenValue[1], -1.0 * eigenValue[0]);
            if ( normalizeValue > 0 )
            {
                double lineMeasure;
                if ( eigenValue[2] <= 0 )
                {
                    lineMeasure = std::exp( -0.5 * itk::Math::sqr( eigenValue[2] / ( m_Param1 * normalizeValue ) ) );
                }
                else
                {
                    lineMeasure = std::exp( -0.5 * itk::Math::sqr( eigenValue[2] / ( m_Param2 * normalizeValue ) ) );
                }
                lineMeasure *= normalizeValue;
                iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
            }
            else
            {
                iter_vesselness_output.Set(NumericTraits< OutputPixelType >::ZeroValue());
            }
            ++iter_hessian_input;
            ++iter_vesselness_output;
        }
        break;

    case Frangi:
        {
            double invNegTwoAlpha2 = -2.0*m_Param2*m_Param2;
            double invNegTwoBeta2 = -2.0*m_Param3*m_Param3;
            double invNegTwoC2 = -2.0*m_Param1*m_Param1;
            symmetricEigenSystem.SetOrderEigenMagnitudes(true);
            while ( !iter_hessian_input.IsAtEnd() )
            {
                symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
                if(eigenValue[1] > 0 || eigenValue[2] > 0)
                {
                   iter_vesselness_output.Set(NumericTraits< OutputPixelType >::ZeroValue());
                }
                else
                {
                    double RB = fabs(eigenValue[0])/sqrt(fabs(eigenValue[1]*eigenValue[2]));
                    double RA = fabs(eigenValue[1])/fabs(eigenValue[2]);
                    double S = sqrt(eigenValue[0]*eigenValue[0] + eigenValue[1]*eigenValue[1] + eigenValue[2]*eigenValue[2]);
                    double lineMeasure = (1.0-exp(RA*RA/invNegTwoAlpha2))*exp(RB*RB*invNegTwoBeta2)*(1.0-exp(S*S*invNegTwoC2));
                    iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
                }
                ++iter_hessian_input;
                ++iter_vesselness_output;
            }
        }
        break;

    case Erdt:
        symmetricEigenSystem.SetOrderEigenValues(true);
        while ( !iter_hessian_input.IsAtEnd() )
        {
            symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
            double K = (fabs(eigenValue[1]) - fabs(eigenValue[0])); K = 1.0 - fabs(K)/K;
            double lineMeasure = K*(((double)2.0/3.0)*eigenValue[2]-eigenValue[1]-eigenValue[0]);
            iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
            ++iter_hessian_input;
            ++iter_vesselness_output;
        }
        break;

    case SatoXY:
        symmetricEigenSystem.SetOrderEigenValues(true);
        while ( !iter_hessian_input.IsAtEnd() )
        {
            symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
            double dot = fabs(eigenValue[0]) > fabs(eigenValue[2])
                         ? 1.0 - fabs(eigenVector[0][2])
                         : 1.0 - fabs(eigenVector[2][2]);

            double normalizeValue = std::min(-1.0 * eigenValue[1], -1.0 * eigenValue[0]);
            if ( normalizeValue > 0 )
            {
                double lineMeasure;
                if ( eigenValue[2] <= 0 )
                {
                    lineMeasure = std::exp( -0.5 * itk::Math::sqr( eigenValue[2] / ( m_Param1 * normalizeValue ) ) );
                }
                else
                {
                    lineMeasure = std::exp( -0.5 * itk::Math::sqr( eigenValue[2] / ( m_Param2 * normalizeValue ) ) );
                }
                lineMeasure *= normalizeValue;
                lineMeasure *= dot;
                iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
            }
            else
            {
                iter_vesselness_output.Set(NumericTraits< OutputPixelType >::ZeroValue());
            }
            ++iter_hessian_input;
            ++iter_vesselness_output;
        }
        break;

    case FrangiXY:
        {
            double invNegTwoAlpha2 = -2.0*m_Param2*m_Param2;
            double invNegTwoBeta2 = -2.0*m_Param3*m_Param3;
            double invNegTwoC2 = -2.0*m_Param1*m_Param1;
            symmetricEigenSystem.SetOrderEigenMagnitudes(true);
            while ( !iter_hessian_input.IsAtEnd() )
            {
                symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
                double dot = 1.0 - fabs(eigenVector[0][2]);

                if(eigenValue[1] > 0 || eigenValue[2] > 0)
                {
                   iter_vesselness_output.Set(NumericTraits< OutputPixelType >::ZeroValue());
                }
                else
                {
                    double RB = fabs(eigenValue[0])/sqrt(fabs(eigenValue[1]*eigenValue[2]));
                    double RA = fabs(eigenValue[1])/fabs(eigenValue[2]);
                    double S = sqrt(eigenValue[0]*eigenValue[0] + eigenValue[1]*eigenValue[1] + eigenValue[2]*eigenValue[2]);
                    double lineMeasure = (1.0-exp(RA*RA/invNegTwoAlpha2))*exp(RB*RB*invNegTwoBeta2)*(1.0-exp(S*S*invNegTwoC2));

                    lineMeasure *= dot;
                    iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
                }
                ++iter_hessian_input;
                ++iter_vesselness_output;
            }

        }
        break;

    case ErdtXY:
        symmetricEigenSystem.SetOrderEigenValues(true);
        double dot = fabs(eigenValue[0]) > fabs(eigenValue[2])
                     ? 1.0 - fabs(eigenVector[0][2])
                     : 1.0 - fabs(eigenVector[2][2]);
        while ( !iter_hessian_input.IsAtEnd() )
        {
            symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
            double K = (fabs(eigenValue[1]) - fabs(eigenValue[0])); K = 1.0 - fabs(K)/K;
            double lineMeasure = K*(((double)2.0/3.0)*eigenValue[2]-eigenValue[1]-eigenValue[0]);
            lineMeasure *= dot;
            iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
            ++iter_hessian_input;
            ++iter_vesselness_output;
        }
        break;
    }
}

template< typename TPixel >
void
ScaffoldHessianToVesselnessFilter< TPixel >
::PrintSelf(std::ostream & os, Indent indent) const
{
    Superclass::PrintSelf(os, indent);
    switch(m_Method)
    {
    case SatoXY:
        os << indent << "XY enhanced" << std::endl;
    case Sato:
    {
        os << indent << "Method by Sato" << std::endl;
        os << indent << indent << "Alpha1: " << m_Param1 << std::endl;
        os << indent << indent << "Alpha2: " << m_Param2 << std::endl;
    }break;
    case FrangiXY:
        os << indent << "XY enhanced" << std::endl;
    case Frangi:
    {
        os << indent << "Method by Frangi" << std::endl;
        os << indent << indent << "c: " << m_Param1 << std::endl;
        os << indent << indent << "Alpha: " << m_Param2 << std::endl;
        os << indent << indent << "Beta: " << m_Param3 << std::endl;

    }break;
    case ErdtXY:
        os << indent << "XY enhanced" << std::endl;
    case Erdt:
    {
        os << indent << "Method by Erdt" << std::endl;
    }break;

    default: os << indent << "Method: Unspecified or new implementation under test" << std::endl;
    }
}
} // end namespace itk

#endif
