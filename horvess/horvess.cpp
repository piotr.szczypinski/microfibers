/*
 * horvess
 * Optical microscopy of fiber scaffolds and bacteria image preprocessing.
 * Intended for enhancement of fiber structures and noise reduction.
 * Requires Insight Toolkit (ITK) version 4.20 or higher.
 *
 * Copyright (c) 2016 by Piotr M. Szczypiński
 * and NIST (National Institute of Standards and Technology)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of the NIST shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization from the NIST.
 */

#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
//#include <itkAdditiveGaussianNoiseImageFilter.h>
//#include <itkConvolutionImageFilter.h>
//#include <itkFFTConvolutionImageFilter.h>
#include <itkHessianRecursiveGaussianImageFilter.h>
#include <itkDiscreteGaussianImageFilter.h>
#include "itkScaffoldHessianToVesselnessFilter.h"

#include "getoption.h"

char* mode = (char*)"Frangi";
char* input_image = (char*)"input.nii";
char* output_data = (char*)"output.nii";
bool suppress = false;
bool isprinthelp = false;
int scales = 1;
float min_stdev = 1.0;
float max_stdev = 1.0;
float power = 1.0;

typedef itk::Image<float, 3>  ImageType;
typedef itk::ImageFileReader<ImageType> ReaderType;
typedef itk::ImageFileWriter<ImageType> WriterType;

//=============================================================================

ImageType::Pointer vesselness(ImageType::Pointer input_image, float sigma)
{
    time_t curtime;
    time(&curtime);
    printf("  ---- Starts vesselness computation scale %f on %s", sigma, ctime(&curtime)); fflush(stdout);

    typedef itk::HessianRecursiveGaussianImageFilter< ImageType > F1;
    typedef F1::Pointer P;
    typedef itk::ScaffoldHessianToVesselnessFilter< float > F2;
    typedef F2::Pointer P2;
    typedef itk::DiscreteGaussianImageFilter< ImageType, ImageType > F3;
    typedef F3::Pointer P3;

    if(strcmp(mode, "Gauss")==0)
    {
        time(&curtime);
        printf("    -- Gaussian filtering starts on %s", ctime(&curtime)); fflush(stdout);

        P3 filter = F3::New();
        filter->SetInput(input_image);
        filter->SetVariance(sigma);
        filter->Update();
        ImageType::Pointer ret = filter->GetOutput();
        time(&curtime);
        printf("  ---- Completed for sigma=%f on %s", sigma, ctime(&curtime)); fflush(stdout);
        return ret;
    }

    P filter = F1::New();
    filter->SetInput(input_image);
    filter->SetSigma(sigma);
    filter->Update();

    time(&curtime);
    printf("    -- Hessian computation completed on %s", ctime(&curtime)); fflush(stdout);

    P2 filter2 = F2::New();
    filter2->SetInput(filter->GetOutput());

    if(strcmp(mode, "Sato")==0)
    {
        if(suppress)
        {
            filter2->SetMethodSatoXY();
        }
        else filter2->SetMethodSato();
    }
    else if(strcmp(mode, "Erdt")==0)
    {
        if(suppress) filter2->SetMethodErdtXY();
        else filter2->SetMethodErdt();
    }
    else //if(strcmp(mode, "Frangi"))
    {
        if(suppress) filter2->SetMethodFrangiXY(1.0);
        else filter2->SetMethodFrangi(1.0);
    }

    const char* method;
    switch(filter2->GetMethod())
    {
    case F2::SatoXY: method = "SatoXY"; break;
    case F2::Sato: method = "Sato"; break;
    case F2::FrangiXY: method = "FrangiXY"; break;
    case F2::Frangi: method = "Frangi"; break;
    case F2::ErdtXY: method = "ErdtXY"; break;
    case F2::Erdt: method = "Erdt"; break;
    default: method = "Unspecified defaults to Frangi";
    }

    time(&curtime);
    printf("    -- Starts %s vesselness computation on %s", method, ctime(&curtime)); fflush(stdout);
    fflush(stdout);

    filter2->Update();
    ImageType::Pointer ret = filter2->GetOutput();
    time(&curtime);
    printf("  ---- Completed for sigma=%f on %s", sigma, ctime(&curtime)); fflush(stdout);

    return ret;
}

//=============================================================================
ImageType::Pointer multiscaleVesselness(ImageType::Pointer input)
{
    typedef itk::ImageRegionConstIterator< ImageType > ConstIteratorType;
    typedef itk::ImageRegionIterator< ImageType > IteratorType;

    printf("------ Starts multiscale enhancement for %i scales from stdevmin=%f to stdevmax=%f\n", scales, min_stdev, max_stdev); fflush(stdout);

    ImageType::Pointer first = vesselness(input, min_stdev);
    ImageType::Pointer next;
    double q = pow((double)max_stdev/min_stdev,(1.0/double(scales-1)));
    for(int i = 1; i < scales; i++)
    {
        float stdev;
        stdev=min_stdev*pow(q, (double)i);
        next = vesselness(input, stdev*stdev);

        double multip = stdev/min_stdev;
        multip = pow(multip, power);
        IteratorType firstIterator(first, first->GetRequestedRegion());
        ConstIteratorType nextIterator(next, next->GetRequestedRegion());
        firstIterator.GoToBegin();
        nextIterator.GoToBegin();

        while (!firstIterator.IsAtEnd())
        {
            if (multip*nextIterator.Get() > firstIterator.Get())
                firstIterator.Set(multip*nextIterator.Get());
            ++firstIterator;
            ++nextIterator;
        }
    }
    printf("------ Completed multiscale enhancement\n"); fflush(stdout);
    return first;
}

//=============================================================================

int scan_parameters(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-m", "--mode", mode)
        else GET_NOARG_OPTION("-z", "--supress-z", suppress, true)
        else GET_STRING_OPTION("-i", "--input", input_image)
        else GET_INT_OPTION("-s", "--scales", scales)
        else GET_FLOAT_OPTION("-l", "--min_stdev", min_stdev)
        else GET_FLOAT_OPTION("-h", "--max_stdev", max_stdev)
        else GET_FLOAT_OPTION("-p", "--power", power)
        else GET_STRING_OPTION("-o", "--output", output_data)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

void printhelp(void)
{
//------------------------------------------------------------------------------------------v
    printf("Usage: horvess [OPTIONS]...\n");
    printf("Computes vesselness of 3D data. Can suppress structures elongated in Z direction.\n");
    printf("2016.08.08 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -m, --mode <mode>       Set <mode> to: 'Frangi', 'Sato', 'Erdt' or 'Gauss'\n");
    printf("  -z, --suppress-z        Suppress structures elongated in Z direction (ignored with -m Gauss),\n");
    printf("  -s, --scales            Number of scales,\n");
    printf("  -l, --min_stdev         Minimum standard deviation (first scale) for filtration,\n");
    printf("  -h, --max_stdev         Maximum standard deviation (last scale) for filtration,\n");
    printf("  -i, --input <file>      Load 3D image from <file>,\n");
    printf("  -o, --output <file>     Save results to <file>,\n");
    printf("  /?, --help              Display this help and exit\n\n");

    printf("Example: horvess -i input.nii -o output.nii -m Frangi -z -s 3 -l 0.3 -h 3.0\n\n");
}


//=============================================================================
// Program starts in main()
int main(int argc, char *argv[])
{
    ImageType::Pointer input;
    ReaderType::Pointer reader = ReaderType::New();

    int ret = scan_parameters(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp();
        return ret;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try horvess --help for more information.\n");
        fflush(stdout);
        return ret;
    }

    reader->SetFileName(input_image);
    input = reader->GetOutput();
    if(input.IsNull())
    {
        printf("Cannot load from %s\n", input_image); fflush(stdout);
        return -1;
    }

    printf("------ Vesselness starts for %s\n", input_image);
    fflush(stdout);
    ImageType::Pointer output;
    if(scales <= 1) output = vesselness(input, min_stdev);
    else output = multiscaleVesselness(input);

    WriterType::Pointer writer = WriterType::New();
    writer->SetFileName(output_data);
    writer->SetInput(output);
    writer->Update();
    printf("------ Completed and saved to %s\n", output_data); fflush(stdout);

    return EXIT_SUCCESS;
}


