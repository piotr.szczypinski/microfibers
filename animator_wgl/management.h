#ifndef MANAGEMENTGL_H
#define MANAGEMENTGL_H

#include "imagegl.h"
#include "byteimage.h"
#include <nifti/nifti1.h>
#include <nifti/nifti1_io.h>
#include <opencv2/opencv.hpp>

extern char* red_image;
extern char* green_image;
extern char* blue_image;
extern char* video_file;
extern int frames_to_save;
extern int frame_width;
extern int frame_height;
extern char* fourcc_code;
extern float frames_per_second;
extern int rotation_mode;
extern float rescaling;
extern float weightRed;
extern float weightGreen;
extern float weightBlue;  //Enhance blue channel
extern float bias;

class Management
{
public:
    Management();
    void renderingLoop(void);
    bool animate;

private:
    Byte3DImage* red;
    Byte3DImage* green;
    Byte3DImage* blue;
    Byte3DImage* alpha;
    ImageGl* renderer;
    cv::VideoWriter writer;
    bool loadAndSetImages(void);
    bool generateAlpha(void);
    bool loadNifti(const char* filename, Byte3DImage* image, unsigned int frame = 0);
    void rotationModel(unsigned int frame, double rotation[3]);
    unsigned char* buffer;
};

#endif // MANAGEMENTGL_H
