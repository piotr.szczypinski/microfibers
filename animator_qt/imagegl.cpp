#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

//#include <GL/gl.h>
//#include <GL/glu.h>
#include "imagegl.h"

#ifdef _WIN32
#define lround(x) int(x+0.5)
#endif

#define FRUSTUM_FRONT_DISTANCE 30.0f
#define FRUSTUM_BACK_DISTANCE 3000.0f
#define FRUSTUM_SIDE_TO_SIDE 5.0f
#define CENTER_OF_PROJECTION_DISTANCE 100.0f
//#define USE_ALPHA_CHANNEL_IN_OPENGL



void ImageGl::setIdentityMatrix(GLdouble W[16])
{
    W[0] = 1; W[1] = 0; W[2] = 0; W[3] = 0;
    W[4] = 0; W[5] = 1; W[6] = 0; W[7] = 0;
    W[8] = 0; W[9] = 0; W[10] = 1; W[11] = 0;
    W[12] = 0; W[13] = 0; W[14] = 0; W[15] = 1;
}
//---------------------------------------------------------------------------
void ImageGl::copy4to4(GLdouble W[14], GLdouble W1[14])
{
    for(int k =0; k<16; k++) W[k]=W1[k];
}


void ImageGl::shift(int qx, int qy)
{
//    if(uninverted)invert3(mat_1, mat);
//    uninverted = false;
//    mat[12] += (mat_1[4]*qy+mat_1[0]*qx);
//    mat[13] += (mat_1[5]*qy+mat_1[1]*qx);
//    mat[14] += (mat_1[6]*qy+mat_1[2]*qx);
    modelViewMat[12] -= ((qx*modelViewMat[14])/1000);
    modelViewMat[13] -= ((qy*modelViewMat[14])/1000);
}



//---------------------------------------------------------------------------
void ImageGl::rotate(bool d, int l1, int l2)
{
    GLdouble a, b;
    for(int k =0; k<12; k+=4)
    {
        a = cosa * modelViewMat[k + l1] + (d ?sina :-sina) * modelViewMat[k + l2];
        b = (d ?-sina :sina) * modelViewMat[k + l1] + cosa * modelViewMat[k + l2];
        modelViewMat[k + l1] = a;
        modelViewMat[k + l2] = b;
    }
}

//---------------------------------------------------------------------------
void ImageGl::rotatem(double rot, int l1, int l2, GLdouble W[14])
{
    GLdouble a, b;
    GLdouble sina = sin(rot);
    GLdouble cosa = cos(rot);

    for(int k =0; k<12; k+=4)
    {
        a = cosa * W[k + l1] + sina * W[k + l2];
        b = -sina * W[k + l1] + cosa * W[k + l2];
        W[k + l1] = a;
        W[k + l2] = b;
    }
}

/*------------------------------------------------------------------------*/
GLdouble ImageGl::determinant(GLdouble W[14])
{
    return
            W[0*4+0]*
            (
            W[1*4+1]*(W[2*4+2]*W[3*4+3]-W[2*4+3]*W[3*4+2])
            -W[1*4+2]*(W[2*4+1]*W[3*4+3]-W[2*4+3]*W[3*4+1])
            +W[1*4+3]*(W[2*4+1]*W[3*4+2]-W[2*4+2]*W[3*4+1])
            )
            -W[0*4+1]*
            (
            W[1*4+0]*(W[2*4+2]*W[3*4+3]-W[2*4+3]*W[3*4+2])
            -W[1*4+2]*(W[2*4+0]*W[3*4+3]-W[2*4+3]*W[3*4+0])
            +W[1*4+3]*(W[2*4+0]*W[3*4+2]-W[2*4+2]*W[3*4+0])
            )
            +W[0*4+2]*
            (
            W[1*4+0]*(W[2*4+1]*W[3*4+3]-W[2*4+3]*W[3*4+1])
            -W[1*4+1]*(W[2*4+0]*W[3*4+3]-W[2*4+3]*W[3*4+0])
            +W[1*4+3]*(W[2*4+0]*W[3*4+1]-W[2*4+1]*W[3*4+0])
            )
            -W[0*4+3]*
            (
            W[1*4+0]*(W[2*4+1]*W[3*4+2]-W[2*4+2]*W[3*4+1])
            -W[1*4+1]*(W[2*4+0]*W[3*4+2]-W[2*4+2]*W[3*4+0])
            +W[1*4+2]*(W[2*4+0]*W[3*4+1]-W[2*4+1]*W[3*4+0])
            );
}

/*------------------------------------------------------------------------*/
GLdouble ImageGl::subdet3of4(GLdouble W[16], int w, int k)
{
    int w0, w1, w2;
    int k0, k1, k2;

    w0=0; if(w0==w) w0++;
    w1=w0+1; if(w1==w) w1++;
    w2=w1+1; if(w2==w) w2++;

    k0=0; if(k0==k) k0++;
    k1=k0+1; if(k1==k) k1++;
    k2=k1+1; if(k2==k) k2++;

    return
            W[w0*4+k0]*(W[w1*4+k1]*W[w2*4+k2]-W[w1*4+k2]*W[w2*4+k1])
            -W[w0*4+k1]*(W[w1*4+k0]*W[w2*4+k2]-W[w1*4+k2]*W[w2*4+k0])
            +W[w0*4+k2]*(W[w1*4+k0]*W[w2*4+k1]-W[w1*4+k1]*W[w2*4+k0]);
}

/*------------------------------------------------------------------------*/
GLdouble ImageGl::invert3(GLdouble I[16], GLdouble W[16])
{
    GLdouble det =
            W[0]*(W[5]*W[10]-W[6]*W[9])
            +W[1]*(W[6]*W[8]-W[4]*W[10])
            +W[2]*(W[4]*W[9]-W[5]*W[8]);

    if(det==0) return 0;
    I[0] = (W[5]*W[10]-W[6]*W[9])/det;
    I[4] = (W[6]*W[8]-W[4]*W[10])/det;
    I[8] = (W[4]*W[9]-W[5]*W[8])/det;
    I[12] = 0.0;
    I[1] = (W[2]*W[9]-W[1]*W[10])/det;
    I[5] = (W[0]*W[10]-W[2]*W[8])/det;
    I[9] = (W[1]*W[8]-W[0]*W[9])/det;
    I[13] = 0.0;
    I[2] = (W[1]*W[6]-W[2]*W[5])/det;
    I[6] = (W[2]*W[4]-W[0]*W[6])/det;
    I[10] = (W[0]*W[5]-W[1]*W[4])/det;
    I[14] = 0.0;
    I[3] = 0.0;
    I[7] = 0.0;
    I[11] = 0.0;
    I[15] = 1.0;
    return det;
}


/*------------------------------------------------------------------------*/
int ImageGl::invert4(GLdouble W[16], GLdouble W1[16])
{
    int k, l;
    GLdouble det;
    det=determinant(W1);
    if(det==0) return 1;

    for(k=0; k<4; k++)
        for(l=0; l<4; l++)
        {
            if((k+l)%2) W[l*4+k]=-subdet3of4(W1, k, l)/det;
            else W[l*4+k]=subdet3of4(W1, k, l)/det;
        }
    return 0;
}

/*------------------------------------------------------------------------*/
void ImageGl::multiply4x4(GLdouble W[16], GLdouble W1[16], GLdouble W2[16])
{
    for(int i1 = 0; i1 < 4; i1++)
        for(int i2 = 0; i2 < 4; i2++)
            W[i1*4+i2] = W2[i1*4]*W1[i2]
                    + W2[i1*4+1]*W1[i2+4]
                    + W2[i1*4+2]*W1[i2+8]
                    + W2[i1*4+3]*W1[i2+12];
}

/*------------------------------------------------------------------------*/
void ImageGl::multiply3x3(GLdouble W[16], GLdouble W1[16], GLdouble W2[16])
{
    for(int i1 = 0; i1 < 3; i1++)
        for(int i2 = 0; i2 < 3; i2++)
            W[i1*4+i2] = W1[i1*4]*W2[i2]+ W1[i1*4+1]*W2[i2+4]+ W1[i1*4+2]*W2[i2+8];
}


ImageGl::ImageGl(QWidget *parent, unsigned int width, unsigned int height):
    QGLWidget(parent, 0, 0)
{
    this->pwidth = width;
    this->pheight = height;

//    for(int n = 0; n < 4; n++) renderlist[n] = 0;
    setIdentityMatrix(modelViewMat);
    modelViewMat[14] = -CENTER_OF_PROJECTION_DISTANCE;
//    setIdentityMatrix(mato);
//    setIdentityMatrix(mat_1);
//    coars = true;
    freeze = false;
//    coarse = true;//false;
    image[0] = NULL;
    image[1] = NULL;
    image[2] = NULL;
    image[3] = NULL;
//    lastdirectionofvolume = 0;

//    channel = 0;
    minwindow = 0;
    maxwindow = 255;
//    visibility = NORMAL;
    zoomlevel = 0;

    sliceindex[0] = 0;
    sliceindex[1] = 0;
    sliceindex[2] = 0;
    renderslice[0] = true;
    renderslice[1] = true;
    renderslice[2] = true;
    rendervolume = false;
    stereo = 0;
    stereodepth = 0;

    for(int d = 0; d < 4; d++) lastdirectionofvolume[d] = 0;

    //initializeGL();
}

void ImageGl::ReadPixels(unsigned int width, unsigned int height, GLubyte* buffer)
{
    if(buffer) glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
}

void ImageGl::initializeGL()
{
    initializeGLFunctions();

    // Wartosci i wspolrzedne zrodel swiatla
    GLfloat  diffuseLight[] = {0.7f, 0.7f, 0.7f, 1.0f };
    GLfloat  specular[] = { 0.7f, 0.7f, 0.7f, 1.0f};
    GLfloat  lightPos[] = { 500.0f, -2000.0f, 2000.0f, 1.0f };
    GLfloat  ambientLight[] = {0.3f, 0.3f, 0.3f, 1.0f };
    GLfloat  specref[] =  { 0.4f, 0.4f, 0.4f, 1.0f };

    glEnable(GL_DEPTH_TEST); // Wlaczenie metdy bufora glebokosci
    glEnable(GL_CULL_FACE); // Wlaczenie metody eliminowania niewidocznych scianek
    glEnable(GL_LIGHTING); // Wlaczenie metody obliczania oswietlenia
    //glEnable(GL_NORMALIZE); // Ustawienie automatycznego obliczania normalnych do powierzchni
    //glEnable(GL_AUTO_NORMAL);

    glEnable(GL_RESCALE_NORMAL);

    //glDisable(GL_NORMALIZE);
    //glDisable(GL_AUTO_NORMAL);

    // Przygotowanie i wlaczenie swiatla GL_LIGHT0
    //  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambientLight);

    glLightfv(GL_LIGHT0,GL_AMBIENT,ambientLight);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuseLight);
    glLightfv(GL_LIGHT0,GL_SPECULAR,specular);
    glLightfv(GL_LIGHT0,GL_POSITION,lightPos);
    glEnable(GL_LIGHT0);

    // Ustawienie wlaciwosci odbijania swiatla dla materialu
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specref);
    glMateriali(GL_FRONT,GL_SHININESS,64);


    backgroundcolor = 0;

    glClearColor((float)((backgroundcolor>>16) & 0xff)/255.0,
                 (float)((backgroundcolor>>8) & 0xff)/255.0,
                 (float)((backgroundcolor) & 0xff)/255.0,
                 0.0f );


    glDisable(GL_BLEND);

    volumelist = glGenLists(2);
}


void ImageGl::paintGLSetMatrices(GLdouble modelview[16], GLdouble projection[16], GLint viewport[4])
{
    GLdouble rangeh, rangev;
    rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)pheight/(double)pwidth);
    rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)pwidth/(double)pheight);
    glViewport(0, 0, pwidth, pheight);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-rangev, rangev, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixd(modelViewMat);
    if(modelview != NULL) glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    if(projection != NULL) glGetDoublev(GL_PROJECTION_MATRIX, projection);
    if(viewport != NULL) glGetIntegerv(GL_VIEWPORT, viewport);
}

void ImageGl::paintGL()
{
    if(pheight<=0 || pwidth<=0) return;

    glColorMask(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GLdouble rangeh, rangev;
    glColor4f((float)((backgroundcolor>>16) & 0xff)/255.0,
                 (float)((backgroundcolor>>8) & 0xff)/255.0,
                 (float)((backgroundcolor) & 0xff)/255.0,
                 1.0f );

    GLdouble stshift = -40 *(double)stereodepth/200.0;
    GLdouble stdepth = -1.5625 *(double)stereodepth/200.0;
    GLdouble mat12 = modelViewMat[12];

    switch(stereo)
    {
    case 1:
        glViewport(0, 0, pwidth, pheight);
        rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)pheight/(double)pwidth);
        rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)pwidth/(double)pheight);
        glColorMask(1, 0, 0, 1);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-rangev-stdepth, rangev-stdepth, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glMatrixMode(GL_MODELVIEW);
        modelViewMat[12]=mat12-stshift;
        glLoadMatrixd(modelViewMat);
        paintSingleGL();

        glColorMask(0, 0, 1, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-rangev+stdepth, rangev+stdepth, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glMatrixMode(GL_MODELVIEW);
        modelViewMat[12]=mat12+stshift;
        glLoadMatrixd(modelViewMat);

        paintSingleGL();
        modelViewMat[12]=mat12;
        glColorMask(1, 1, 1, 1);
        break;

    case 2:
        glViewport(0, 0, pwidth, pheight);
        rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)pheight/(double)pwidth);
        rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)pwidth/(double)pheight);
        //paintGLSetMatrices(false);
        glFrustum(-rangev-stdepth, rangev-stdepth, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glMatrixMode(GL_MODELVIEW);
        modelViewMat[12]=mat12-stshift;
        glLoadMatrixd(modelViewMat);
        glDrawBuffer(GL_BACK_LEFT);
        paintSingleGL();

        glClear(GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-rangev+stdepth, rangev+stdepth, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glMatrixMode(GL_MODELVIEW);
        modelViewMat[12]=mat12+stshift;
        glLoadMatrixd(modelViewMat);
        glDrawBuffer(GL_BACK_RIGHT);
        paintSingleGL();
        modelViewMat[12]=mat12;
        glDrawBuffer(GL_BACK);
        break;

    default:
        paintGLSetMatrices();
//        glMatrixMode(GL_PROJECTION);
//        glLoadIdentity();
//        glFrustum(-rangev, rangev, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
//        glMatrixMode(GL_MODELVIEW);
//        glLoadMatrixd(modelViewMat);
        paintSingleGL();
        break;
    }
    glFlush();

    emit renderingFinished();
}



void ImageGl::paintSingleGL()
{
    int directionofvolume[4];
    bool something = false;

    for(int k = 0; k < 3; k++)
    {
        if(renderslice[k])
        {
            if(createCrossSection(k+1, sliceindex[k])) something = true;
        }
    }

    if(rendervolume)
    {
//        if(coarse)
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_ONE, GL_ONE);
            if(minwindow <= maxwindow)glBlendEquation(GL_FUNC_ADD);
            else glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
            getFacingDirection(directionofvolume);

            if( lastdirectionofvolume[0] == directionofvolume[0] &&
                lastdirectionofvolume[3] == 1 &&
                glIsList(volumelist))
            {
                glCallList(volumelist);
                something = true;
            }
            else
            {
                glNewList(volumelist, GL_COMPILE_AND_EXECUTE);
                if(createVolumetricView(directionofvolume[0])) something = true;
                glEndList();
            }
            lastdirectionofvolume[0] = directionofvolume[0];
            lastdirectionofvolume[1] = directionofvolume[1];
            lastdirectionofvolume[2] = directionofvolume[2];
            lastdirectionofvolume[3] = 1;
            glDisable(GL_BLEND);
        }
//        else
//        {
//            glEnable(GL_BLEND);
//            glBlendFunc(GL_ONE, GL_ONE);
//            if(minwindow <= maxwindow)glBlendEquation(GL_FUNC_ADD);
//            else glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
//            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
//            getFacingDirection(directionofvolume);

//            if( lastdirectionofvolume[0] == directionofvolume[0] &&
//                lastdirectionofvolume[3] == 2 &&
//                glIsList(volumelist))
//            {
//                glCallList(volumelist);
//                something = true;
//            }
//            else
//            {
//                glNewList(volumelist, GL_COMPILE_AND_EXECUTE);
//                if(createVolumetricView(directionofvolume)) something = true;
//                glEndList();
//            }
//            lastdirectionofvolume[0] = directionofvolume[0];
//            lastdirectionofvolume[1] = directionofvolume[1];
//            lastdirectionofvolume[2] = directionofvolume[2];
//            lastdirectionofvolume[3] = 2;
//            glDisable(GL_BLEND);
//        }

    }
}

void ImageGl::rotateView(double z, double y, double x)
{
    rotatem(z, 0, 1, modelViewMat);
    rotatem(x, 0, 2, modelViewMat);
    rotatem(y, 1, 2, modelViewMat);
}

void ImageGl::setVolumeView(bool view)
{
    rendervolume = view;
    glNewList(volumelist, GL_COMPILE);
    glEndList();
    lastdirectionofvolume[3] = 0;
}

void ImageGl::setCrossView(int direction, bool view)
{
    switch(direction)
    {
    case 1: renderslice[0] = view; break;
    case 2: renderslice[1] = view; break;
    case 3: renderslice[2] = view; break;
    }
}

void ImageGl::setBackgroundColor(unsigned int background)
{
    backgroundcolor = background;
    glClearColor((float)((background>>16) & 0xff)/255.0,
                 (float)((background>>8) & 0xff)/255.0,
                 (float)((background) & 0xff)/255.0,
                 1.0f );
}


Byte3DImage* ImageGl::getImage(unsigned int channel)
{
    if(channel >= 4) return NULL;
    return image[channel];
}

void ImageGl::setImage(unsigned int channel, Byte3DImage* rimage)
{
    image[channel] = rimage;
    lastdirectionofvolume[3] = 0;
}


void ImageGl::setStereo(int s, int d)
{
    stereo = s;
    stereodepth = d;
}


void ImageGl::setZoom(double z)
{
    modelViewMat[14] = z;
}


void ImageGl::setZoom(int z)
{
    zoomlevel = z;
    if(zoomlevel > 256) zoomlevel = 256;
    if(zoomlevel < -256) zoomlevel = -256;
    modelViewMat[14] = -(double)FRUSTUM_FRONT_DISTANCE - (((double)FRUSTUM_BACK_DISTANCE-FRUSTUM_FRONT_DISTANCE) * (zoomlevel+256)*(zoomlevel+256))/(513.0*513.0);

}


void ImageGl::setCrossSection(int direction, unsigned int index)
{
    if(abs(direction) > 3 || abs(direction) < 1) return;
    sliceindex[abs(direction-1)] = index;
//    update();
}
void ImageGl::setCrossSection(int direction, bool show)
{
    if(abs(direction) > 3 || abs(direction) < 1) return;

    if(show) renderslice[abs(direction-1)] = true;
    else renderslice[abs(direction-1)] = false;
//    update();
}

void ImageGl::setImageGrayWindowLow(double iminwindow)
{
    minwindow = iminwindow;
    //initView();
    lastdirectionofvolume[3] = 0;
//    update();
}
void ImageGl::setImageGrayWindowHigh(double imaxwindow)
{
    maxwindow = imaxwindow;
    //initView();
    lastdirectionofvolume[3] = 0;
 //   update();
}


void ImageGl::setFalseColor(float level)
{
    float r, g, b;
    if(level <= 0.0) {r = 0.0; g = 0.0; b = 0.0;}
    else if(level < 1.0/6.0) {r = 0.0; g = 0.0; b = 6.0*level;}
    else if(level < 2.0/6.0) {r = 0.0; g = 6.0*(level-1.0/6.0); b = 1.0;}
    else if(level < 3.0/6.0) {r = 6.0*(level-2.0/6.0); g = 1.0; b = 1.0-r;}
    else if(level < 4.0/6.0) {r = 1.0; g = 1.0 - 6.0*(level-3.0/6.0); b = 0.0;}
    else if(level < 5.0/6.0) {r = 1.0; g = 0.0; b = 6.0*(level-4.0/6.0);}
    else if(level < 1.0) {r = 1.0; g = 6.0*(level-5.0/6.0); b = 1.0;}
    else {r = 1.0; g = 1.0; b = 1.0;}
    glColor4f(r, g, b, 1.0f);
}

/**
 * @brief ImageGl::getFacingDirection
 * @param direction
 */
void ImageGl::getFacingDirection(int direction[3])
{
    //if(image[0] != NULL)
    {
        GLdouble x = fabs(modelViewMat[2]);
        GLdouble y = fabs(modelViewMat[6]);
        GLdouble z = fabs(modelViewMat[10]);

        for(int d = 0; d < 3; d++)
        {
            if(x>=z && x>=y)
            {
                if(modelViewMat[2] > 0) direction[d] = 1;
                else direction[d] = -1;

                x = -1;
            }
            else if(y>=z && y>=x)
            {
                if(modelViewMat[6] > 0) direction[d] = 2;
                else direction[d] = -2;

                y = -1;
            }
            else
            {
                if(modelViewMat[10] > 0) direction[d] = 3;
                else direction[d] = -3;

                z = -1;
            }
        }
    }
}

void ImageGl::getMatrix(double m[16])
{
    for(int i = 0; i < 16; i++) m[i] = modelViewMat[i];
}
void ImageGl::setMatrix(double m[16])
{
    for(int i = 0; i < 16; i++) modelViewMat[i] = m[i];
}

bool ImageGl::fillCrossSection(ChannelType* bits, unsigned int stride, unsigned int direction, unsigned int sliceindex, bool noalpha, float aalpha)
{
    unsigned int stepx;
    unsigned int stepy;
    unsigned int sizex;
    unsigned int sizey;
    unsigned int x, y;
    uint8* start;
    unsigned int d;
    Byte3DImage* img = NULL;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    switch(direction)
    {
    case 1: //x
        if(sliceindex >= img->size[0]) return false;
        sizex = img->size[1];
        sizey = img->size[2];
        stepx = img->size[0];
        stepy = stepx*img->size[1];
        start = (uint8*)img->data + sliceindex;
        break;
    case 2: //y
        if(sliceindex >= img->size[1]) return false;
        sizex = img->size[0];
        sizey = img->size[2];
        stepx = 1;
        stepy = img->size[0]*img->size[1];
        start = (uint8*)img->data + img->size[0]*sliceindex;
        break;
    default: //z
        if(sliceindex >= img->size[2]) return false;
        sizex = img->size[0];
        sizey = img->size[1];
        stepx = 1;
        stepy = img->size[0];
        start = (uint8*)img->data + img->size[0]*img->size[1]*sliceindex;
        break;
    }

    int minb = minwindow;
    int maxb = maxwindow;
    uint8* src;
    ChannelType* dst;

    for(d = 0; d < 3; d++)
    {
        if(image[d] == NULL)
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                for(x = 0; x < sizex; x++)
                {
                    *dst = 0; dst+=4;
                }
            }
            continue;
        }

        switch(direction)
        {
        case 1: //x
            start = (uint8*)image[d]->data + sliceindex;
            break;
        case 2: //y
            start = (uint8*)image[d]->data + image[d]->size[0]*sliceindex;
            break;
        default: //z
            start = (uint8*)image[d]->data + image[d]->size[0]*image[d]->size[1]*sliceindex;
            break;
        }


        if(minb <= maxb)
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                src = start + y*stepy;
                for(x = 0; x < sizex; x++)
                {
                    if(*src <= minb) *dst = 0;
                    else if(*src > maxb) *dst = max_brightness_value;
                    else *dst = max_brightness_value*(*src-minb)/(maxb-minb);
                    dst+=4;
                    src+=stepx;
                }
            }
        }
        else
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                src = start + y*stepy;
                for(x = 0; x < sizex; x++)
                {
                    if(*src >= minb) *dst = 0;
                    else if(*src < maxb) *dst = max_brightness_value;
                    else *dst = max_brightness_value*(minb-*src)/(minb-maxb);
                    dst+=4;
                    src+=stepx;
                }
            }
        }
    }

//Alfa
    if(image[3] == NULL || noalpha)
    {
        for(y = 0; y < sizey; y++)
        {
            dst = bits + y*stride + d;
            for(x = 0; x < sizex; x++)
            {
                *dst = max_brightness_value; dst+=4;
            }
        }
    }
    else
    {
        switch(direction)
        {
        case 1: //x
            start = (uint8*)image[3]->data + sliceindex;
            break;
        case 2: //y
            start = (uint8*)image[3]->data + image[3]->size[0]*sliceindex;
            break;
        default: //z
            start = (uint8*)image[3]->data + image[3]->size[0]*image[3]->size[1]*sliceindex;
            break;
        }

        if(minb <= maxb)
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                src = start + y*stepy;
                for(x = 0; x < sizex; x++)
                {
                    if(*src <= minb) *dst = 0;
                    else if(*src > maxb) *dst = max_brightness_value*aalpha;
                    else
                    {
                        *dst = max_brightness_value*aalpha*(*src-minb)/(maxb-minb);
                    }
                    dst+=4;
                    src+=stepx;
                }
            }
        }
        else
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                src = start + y*stepy;
                for(x = 0; x < sizex; x++)
                {
                    if(*src >= minb) *dst = 0;
                    else if(*src < maxb) *dst = max_brightness_value*aalpha;
                    else
                    {
                        *dst = max_brightness_value*aalpha*(minb-*src)/(minb-maxb);
                    }
                    dst+=4;
                    src+=stepx;
                }
            }
        }
    }

    return true;
}


/**
 * @brief ImageGl::createCrossSection
 * @param direction
 * @param sliceindex
 */
bool ImageGl::createCrossSection(unsigned int direction, unsigned int sliceindex)
{
    unsigned int i, d;
    Byte3DImage* img = NULL;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    if(img == NULL) return false;
    unsigned int* size = img->size;
    unsigned int sizeimg[3];
    //GLuint listindex;
    switch(direction)
    {
    case 1: //x
        //listindex = firstlist+1;
        sizeimg[0] = size[1];
        sizeimg[1] = size[2];
        sizeimg[2] = size[0];
        break;
    case 2: //y
        //listindex = firstlist+2;
        sizeimg[0] = size[0];
        sizeimg[1] = size[2];
        sizeimg[2] = size[1];
        break;
    default: //z
        //listindex = firstlist+3;
        sizeimg[0] = size[0];
        sizeimg[1] = size[1];
        sizeimg[2] = size[2];
        break;
    }


    unsigned int sizetex[2];

    for(d = 0; d < 2; d++)
    {
        sizetex[d] = 1;
        for(i = 0; i < sizeof(unsigned int)*8; i++)
        {
            if(sizetex[d] >= sizeimg[d]) break;
            sizetex[d] <<= 1;
        }
    }

    unsigned int stride = sizetex[0]*4;
    ChannelType* bits = new ChannelType[sizetex[0]*sizetex[1]*4];

    //glNewList(listindex, GL_COMPILE);
    glDisable(GL_CULL_FACE);


    unsigned int min_slixces = sizeimg[0];
    if(sizeimg[1] < min_slixces) min_slixces = sizeimg[1];
    if(sizeimg[2] < min_slixces) min_slixces = sizeimg[2];

    float aalpha = (float)min_slixces/(float)sizeimg[2];

    fillCrossSection(bits, stride, direction, sliceindex, true, aalpha);


    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// Force 4-byte alignment
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glDisable(GL_TEXTURE_1D);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glFrontFace(GL_CCW);
    glNormal3f(0.0, 0.0, 0.0);
    renderCrossSection(direction, sliceindex, bits, sizeimg, sizetex);

    delete[] bits;
    return true;
}

/**
 * @brief ImageGl::renderCrossSection
 * @param direction
 * @param sliceindex
 * @param bits
 * @param sizeimg
 * @param sizetex
 * @param stride
 */
void ImageGl::renderCrossSection(unsigned int direction, unsigned int sliceindex, ChannelType* bits, unsigned int sizeimg[2], unsigned int sizetex[2])
{
    unsigned int d;
    Byte3DImage* img = NULL;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    //pms_nist glTexImage2D(GL_TEXTURE_2D, 0, 4, sizetex[0], sizetex[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, bits);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, sizetex[0], sizetex[1], 0, GL_RGBA, CHANNEL_TYPE, bits);

    glBegin(GL_QUADS);
    unsigned int* size = img->size;
    GLfloat sx, sy, sz;

    const GLfloat texture_margin = 0.1;

//pms_nist
    switch(direction)
    {
    case 1: //x
        sx = img->voxelsize[0]*((GLfloat)sliceindex - (GLfloat)size[0]/2.0f + 0.5f);
        sy = img->voxelsize[1]*(GLfloat)size[1]/2.0f;
        sz = img->voxelsize[2]*(GLfloat)size[2]/2.0f;
        glTexCoord2f(texture_margin/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(sx, -sy, -sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(sx, sy, -sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(texture_margin/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(sx, -sy, sz);
        break;
    case 2: //y
        sx = img->voxelsize[0]*(GLfloat)size[0]/2.0f;
        sy = img->voxelsize[1]*((GLfloat)sliceindex - (GLfloat)size[1]/2.0f + 0.5f);
        sz = img->voxelsize[2]*(GLfloat)size[2]/2.0f;
        glTexCoord2f(texture_margin/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(-sx, sy, -sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(sx, sy, -sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(texture_margin/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(-sx, sy, sz);
        break;
    default: //z
        sx = img->voxelsize[0]*(GLfloat)size[0]/2.0f;
        sy = img->voxelsize[1]*(GLfloat)size[1]/2.0f;
        sz = img->voxelsize[2]*((GLfloat)sliceindex - (GLfloat)size[2]/2.0f + 0.5f);
        glTexCoord2f(texture_margin/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(-sx, -sy, sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(sx, -sy, sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(texture_margin/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(-sx, sy, sz);
        break;
    }
    glEnd();
}

/**
 * @brief ImageGl::volumetricView creates transparent view of cross-sections similar to MIP
 */
bool ImageGl::createVolumetricView(int direction)
{
    unsigned int d;

    Byte3DImage* img = NULL;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    if(img == NULL) return false;
    unsigned int* size = img->size;
    unsigned int sizeimg[3];

    switch(abs(direction))
    {
    case 1: //x
        sizeimg[0] = size[1];
        sizeimg[1] = size[2];
        sizeimg[2] = size[0];
        break;
    case 2: //y
        sizeimg[0] = size[0];
        sizeimg[1] = size[2];
        sizeimg[2] = size[1];
        break;
    default: //z
        sizeimg[0] = size[0];
        sizeimg[1] = size[1];
        sizeimg[2] = size[2];
        break;
    }

    unsigned int i;
    unsigned int sizetex[2];

    for(d = 0; d < 2; d++)
    {
        sizetex[d] = 1;
        for(i = 0; i < sizeof(unsigned int)*8; i++)
        {
            if(sizetex[d] >= sizeimg[d]) break;
            sizetex[d] <<= 1;
        }
    }
    glDisable(GL_CULL_FACE);

    unsigned int stride = sizetex[0]*4;
    ChannelType* bits = new ChannelType[sizetex[0]*sizetex[1]*4];

    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// Force 4-byte alignment
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glDisable(GL_TEXTURE_1D);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glFrontFace(GL_CCW);
    glNormal3f(0.0, 0.0, 0.0);


    unsigned int min_slixces = sizeimg[0];
    if(sizeimg[1] < min_slixces) min_slixces = sizeimg[1];
    if(sizeimg[2] < min_slixces) min_slixces = sizeimg[2];

    float aalpha = (float)min_slixces/(float)sizeimg[2];
    aalpha = pow(aalpha, 0.5);


    unsigned int sliceindex, sliceindexstart, sliceindexmax, sliceindexstep;
    if(direction > 0)
    {
        sliceindexstart = 0;
        sliceindexstep = 1;
    }
    else
    {
        sliceindexstart = sizeimg[2]-1;
        sliceindexstep = -1;
    }
    sliceindexmax = sizeimg[2];
    for(sliceindex = sliceindexstart; sliceindex < sliceindexmax; sliceindex+=sliceindexstep)
    {
        fillCrossSection(bits, stride, abs(direction), sliceindex, false, aalpha);
        renderCrossSection(abs(direction), sliceindex, bits, sizeimg, sizetex);
    }

    delete[] bits;
    return true;
}

