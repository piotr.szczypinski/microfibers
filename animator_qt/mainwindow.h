#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "imagegl.h"
#include "byteimage.h"
#include <nifti/nifti1.h>
#include <nifti/nifti1_io.h>
#include <opencv2/opencv.hpp>

extern char* red_image;
extern char* green_image;
extern char* blue_image;
extern char* video_file;
extern int frames_to_save;
extern int frame_width;
extern int frame_height;
extern char* fourcc_code;
extern float frames_per_second;
extern int rotation_mode;
extern float rescaling;

extern float weightRed;
extern float weightGreen;
extern float weightBlue;  //Enhance blue channel
extern float bias;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:
    void signalShowFinished();

private slots:
    void processTimer(void);
    void renderingFinished();

private:
    Ui::MainWindow *ui;
    Byte3DImage* red;
    Byte3DImage* green;
    Byte3DImage* blue;
    Byte3DImage* alpha;
    ImageGl* renderer;
    unsigned int size[3];
    double voxelsize[3];
    QTimer timer;
    bool animate;
    cv::VideoWriter writer;
    bool loadAndSetImages(void);
    bool generateAlpha(void);
    bool loadNifti(const char* filename, Byte3DImage* image, unsigned int frame = 0);
    void rotationModel(unsigned int frame, double rotation[3]);

    unsigned char* buffer;
};

#endif // MAINWINDOW_H
