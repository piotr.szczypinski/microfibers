#-------------------------------------------------
#
# Project created by QtCreator 2016-08-31T10:39:43
#
#-------------------------------------------------

QT       += core gui opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = animator
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
        imagegl.cpp 

HEADERS  += mainwindow.h \
            imagegl.h \
    byteimage.h \
    getoption.h

FORMS    += mainwindow.ui

macx{
    INCLUDEPATH += /usr/local/include\
                   /Users/pms3/Sources3rdParty/alglib-3.10.0/cpp/src\
                   /Users/pms3/Sources3rdParty/InsightToolkit-4.10.0/Modules/ThirdParty/MINC/src/libminc\
                   /System/Library/Frameworks/OpenGL.framework/Versions/A/Headers\
                   /usr/local/include/ITK-4.10\
                   /Users/pms3/Sources3rdParty/InsightToolkit-4.10.0/Modules/Nonunit/Review/include

    LIBS += \
            /usr/local/lib/libopencv_core.dylib\
            /usr/local/lib/libopencv_video.dylib\
            /usr/local/lib/libopencv_highgui.dylib\
            /usr/local/lib/libitksys-4.10.a\
            /usr/local/lib/libITKBiasCorrection-4.10.a\
            /usr/local/lib/libITKBioCell-4.10.a\
            /usr/local/lib/libITKCommon-4.10.a\
            /usr/local/lib/libITKDICOMParser-4.10.a\
            /usr/local/lib/libitkdouble-conversion-4.10.a\
            /usr/local/lib/libITKEXPAT-4.10.a\
            /usr/local/lib/libITKFEM-4.10.a\
            /usr/local/lib/libitkhdf5_cpp-4.10.a\
            /usr/local/lib/libitkhdf5-4.10.a\
            /usr/local/lib/libITKIOBioRad-4.10.a\
            /usr/local/lib/libITKIOBMP-4.10.a\
            /usr/local/lib/libITKIOCSV-4.10.a\
            /usr/local/lib/libITKIOGDCM-4.10.a\
            /usr/local/lib/libITKIOGE-4.10.a\
            /usr/local/lib/libITKIOGIPL-4.10.a\
            /usr/local/lib/libITKIOHDF5-4.10.a\
            /usr/local/lib/libITKIOImageBase-4.10.a\
            /usr/local/lib/libITKIOIPL-4.10.a\
            /usr/local/lib/libITKIOJPEG-4.10.a\
            /usr/local/lib/libITKIOLSM-4.10.a\
            /usr/local/lib/libITKIOMesh-4.10.a\
            /usr/local/lib/libITKIOMeta-4.10.a\
            /usr/local/lib/libITKIOMRC-4.10.a\
            /usr/local/lib/libITKIONIFTI-4.10.a\
            /usr/local/lib/libITKIONRRD-4.10.a\
            /usr/local/lib/libITKIOPNG-4.10.a\
            /usr/local/lib/libITKIOSiemens-4.10.a\
            /usr/local/lib/libITKIOSpatialObjects-4.10.a\
            /usr/local/lib/libITKIOStimulate-4.10.a\
            /usr/local/lib/libITKIOTIFF-4.10.a\
            /usr/local/lib/libITKIOTransformBase-4.10.a\
            /usr/local/lib/libITKIOTransformHDF5-4.10.a\
            /usr/local/lib/libITKIOTransformInsightLegacy-4.10.a\
            /usr/local/lib/libITKIOTransformMatlab-4.10.a\
            /usr/local/lib/libITKIOVTK-4.10.a\
            /usr/local/lib/libITKIOXML-4.10.a\
            /usr/local/lib/libITKKLMRegionGrowing-4.10.a\
            /usr/local/lib/libITKLabelMap-4.10.a\
            /usr/local/lib/libITKMesh-4.10.a\
            /usr/local/lib/libITKMetaIO-4.10.a\
            /usr/local/lib/libitkNetlibSlatec-4.10.a\
            /usr/local/lib/libITKniftiio-4.10.a\
            /usr/local/lib/libITKNrrdIO-4.10.a\
            /usr/local/lib/libITKOptimizers-4.10.a\
            /usr/local/lib/libITKPath-4.10.a\
            /usr/local/lib/libITKPolynomials-4.10.a\
            /usr/local/lib/libITKQuadEdgeMesh-4.10.a\
            /usr/local/lib/libITKSpatialObjects-4.10.a\
            /usr/local/lib/libITKStatistics-4.10.a\
            /usr/local/lib/libitkv3p_netlib-4.10.a\
            /usr/local/lib/libitkvcl-4.10.a\
            /usr/local/lib/libITKVideoCore-4.10.a\
            /usr/local/lib/libITKVideoIO-4.10.a\
            /usr/local/lib/libitkvnl_algo-4.10.a\
            /usr/local/lib/libitkvnl-4.10.a\
            /usr/local/lib/libITKVNLInstantiation-4.10.a\
            /usr/local/lib/libITKVTK-4.10.a\
            /usr/local/lib/libITKWatersheds-4.10.a\
            /usr/local/lib/libitkzlib-4.10.a\
            /usr/local/lib/libitktiff-4.10.a\
            /usr/local/lib/libITKznz-4.10.a
}
else:unix{
    INCLUDEPATH += /usr/include/nifti
    LIBS += -lGLU
    LIBS += -lniftiio
    LIBS += -lopencv_core
    LIBS += -lopencv_imgproc
    LIBS += -lopencv_highgui
    LIBS += -lopencv_video
}
else:win32{

#    INCLUDEPATH += \
#        C:/InsightToolkit-4.10.0/Modules/ThirdParty/MINC/src/libminc\
#        C:\InsightToolkit-4.10.0\Modules\ThirdParty\MINC\src\libminc\nifti\
#        C:\InsightToolkit-4.10.0\build\CMakeTmp\
#        C:\opencv\build\include

    INCLUDEPATH += C:/Program/usr/include

    LIBS += advapi32.lib
    LIBS += opengl32.lib
    LIBS += \
        C:\Program\usr\lib\ITKBiasCorrection-4.10.lib\
        C:\Program\usr\lib\ITKBioCell-4.10.lib\
        C:\Program\usr\lib\ITKCommon-4.10.lib\
        C:\Program\usr\lib\ITKDICOMParser-4.10.lib\
        C:\Program\usr\lib\itkdouble-conversion-4.10.lib\
        C:\Program\usr\lib\ITKEXPAT-4.10.lib\
        C:\Program\usr\lib\ITKFEM-4.10.lib\
        C:\Program\usr\lib\itkgdcmcharls-4.10.lib\
        C:\Program\usr\lib\itkgdcmCommon-4.10.lib\
        C:\Program\usr\lib\itkgdcmDICT-4.10.lib\
        C:\Program\usr\lib\itkgdcmDSED-4.10.lib\
        C:\Program\usr\lib\itkgdcmIOD-4.10.lib\
        C:\Program\usr\lib\itkgdcmjpeg12-4.10.lib\
        C:\Program\usr\lib\itkgdcmjpeg16-4.10.lib\
        C:\Program\usr\lib\itkgdcmjpeg8-4.10.lib\
        C:\Program\usr\lib\itkgdcmMEXD-4.10.lib\
        C:\Program\usr\lib\itkgdcmMSFF-4.10.lib\
        C:\Program\usr\lib\itkgdcmopenjpeg-4.10.lib\
        C:\Program\usr\lib\itkgdcmsocketxx-4.10.lib\
        C:\Program\usr\lib\ITKgiftiio-4.10.lib\
        C:\Program\usr\lib\itkhdf5-4.10.lib\
        C:\Program\usr\lib\itkhdf5_cpp-4.10.lib\
        C:\Program\usr\lib\ITKIOBioRad-4.10.lib\
        C:\Program\usr\lib\ITKIOBMP-4.10.lib\
        C:\Program\usr\lib\ITKIOCSV-4.10.lib\
        C:\Program\usr\lib\ITKIOGDCM-4.10.lib\
        C:\Program\usr\lib\ITKIOGE-4.10.lib\
        C:\Program\usr\lib\ITKIOGIPL-4.10.lib\
        C:\Program\usr\lib\ITKIOHDF5-4.10.lib\
        C:\Program\usr\lib\ITKIOImageBase-4.10.lib\
        C:\Program\usr\lib\ITKIOIPL-4.10.lib\
        C:\Program\usr\lib\ITKIOJPEG-4.10.lib\
        C:\Program\usr\lib\ITKIOLSM-4.10.lib\
        C:\Program\usr\lib\ITKIOMesh-4.10.lib\
        C:\Program\usr\lib\ITKIOMeta-4.10.lib\
        C:\Program\usr\lib\ITKIOMRC-4.10.lib\
        C:\Program\usr\lib\ITKIONIFTI-4.10.lib\
        C:\Program\usr\lib\ITKIONRRD-4.10.lib\
        C:\Program\usr\lib\ITKIOPNG-4.10.lib\
        C:\Program\usr\lib\ITKIOSiemens-4.10.lib\
        C:\Program\usr\lib\ITKIOSpatialObjects-4.10.lib\
        C:\Program\usr\lib\ITKIOStimulate-4.10.lib\
        C:\Program\usr\lib\ITKIOTIFF-4.10.lib\
        C:\Program\usr\lib\ITKIOTransformBase-4.10.lib\
        C:\Program\usr\lib\ITKIOTransformHDF5-4.10.lib\
        C:\Program\usr\lib\ITKIOTransformInsightLegacy-4.10.lib\
        C:\Program\usr\lib\ITKIOTransformMatlab-4.10.lib\
        C:\Program\usr\lib\ITKIOVTK-4.10.lib\
        C:\Program\usr\lib\ITKIOXML-4.10.lib\
        C:\Program\usr\lib\itkjpeg-4.10.lib\
        C:\Program\usr\lib\ITKKLMRegionGrowing-4.10.lib\
        C:\Program\usr\lib\ITKLabelMap-4.10.lib\
        C:\Program\usr\lib\ITKMesh-4.10.lib\
        C:\Program\usr\lib\ITKMetaIO-4.10.lib\
        C:\Program\usr\lib\itknetlib-4.10.lib\
        C:\Program\usr\lib\itkNetlibSlatec-4.10.lib\
        C:\Program\usr\lib\ITKniftiio-4.10.lib\
        C:\Program\usr\lib\ITKNrrdIO-4.10.lib\
        C:\Program\usr\lib\ITKOptimizers-4.10.lib\
        C:\Program\usr\lib\ITKOptimizersv4-4.10.lib\
        C:\Program\usr\lib\ITKPath-4.10.lib\
        C:\Program\usr\lib\itkpng-4.10.lib\
        C:\Program\usr\lib\ITKPolynomials-4.10.lib\
        C:\Program\usr\lib\ITKQuadEdgeMesh-4.10.lib\
        C:\Program\usr\lib\ITKSpatialObjects-4.10.lib\
        C:\Program\usr\lib\ITKStatistics-4.10.lib\
        C:\Program\usr\lib\itksys-4.10.lib\
        C:\Program\usr\lib\itktestlib-4.10.lib\
        C:\Program\usr\lib\itktiff-4.10.lib\
        C:\Program\usr\lib\ITKTransform-4.10.lib\
        C:\Program\usr\lib\itkv3p_netlib-4.10.lib\
        C:\Program\usr\lib\itkvcl-4.10.lib\
        C:\Program\usr\lib\ITKVideoCore-4.10.lib\
        C:\Program\usr\lib\ITKVideoIO-4.10.lib\
        C:\Program\usr\lib\itkvnl-4.10.lib\
        C:\Program\usr\lib\ITKVNLInstantiation-4.10.lib\
        C:\Program\usr\lib\itkvnl_algo-4.10.lib\
        C:\Program\usr\lib\ITKVTK-4.10.lib\
        C:\Program\usr\lib\ITKWatersheds-4.10.lib\
        C:\Program\usr\lib\itkzlib-4.10.lib\
        C:\Program\usr\lib\ITKznz-4.10.lib\
        C:\Program\usr\lib\opencv_calib3d2413.lib\
        C:\Program\usr\lib\opencv_contrib2413.lib\
        C:\Program\usr\lib\opencv_core2413.lib\
        C:\Program\usr\lib\opencv_features2d2413.lib\
        C:\Program\usr\lib\opencv_flann2413.lib\
        C:\Program\usr\lib\opencv_gpu2413.lib\
        C:\Program\usr\lib\opencv_haartraining_engine.lib\
        C:\Program\usr\lib\opencv_highgui2413.lib\
        C:\Program\usr\lib\opencv_imgproc2413.lib\
        C:\Program\usr\lib\opencv_legacy2413.lib\
        C:\Program\usr\lib\opencv_ml2413.lib\
        C:\Program\usr\lib\opencv_nonfree2413.lib\
        C:\Program\usr\lib\opencv_objdetect2413.lib\
        C:\Program\usr\lib\opencv_ocl2413.lib\
        C:\Program\usr\lib\opencv_photo2413.lib\
        C:\Program\usr\lib\opencv_stitching2413.lib\
        C:\Program\usr\lib\opencv_superres2413.lib\
        C:\Program\usr\lib\opencv_ts2413.lib\
        C:\Program\usr\lib\opencv_video2413.lib\
        C:\Program\usr\lib\opencv_videostab2413.lib
}

