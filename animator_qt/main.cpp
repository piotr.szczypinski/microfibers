#include "mainwindow.h"
#include <QApplication>

#include "getoption.h"

char* red_image = NULL;
char* green_image = NULL;
char* blue_image = NULL;
char* video_file = (char*)"output.mov";
char* fourcc_code = NULL;
float frames_per_second = 15.0;
int frames_to_save = 0;
int frame_width = 640;
int frame_height = 480;
int rotation_mode = 0;
float rescaling = 400;
bool isprinthelp = false;
float weightRed = 0.33;
float weightGreen = 0.33;
float weightBlue = 0.33;  //Enhance blue channel
float bias = 0.0;


//=============================================================================

int scan_parameters(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_INT_OPTION("-w", "--frame-width", frame_width)
        else GET_INT_OPTION("-h", "--frame-height", frame_height)
        else GET_STRING_OPTION("-r", "--red-image", red_image)
        else GET_STRING_OPTION("-g", "--green-image", green_image)
        else GET_STRING_OPTION("-b", "--blue-image", blue_image)
        else GET_FLOAT_OPTION("-R", "--red-weight", weightRed)
        else GET_FLOAT_OPTION("-G", "--green-weight", weightGreen)
        else GET_FLOAT_OPTION("-B", "--blue-weight", weightBlue)
        else GET_STRING_OPTION("-v", "--video-file", video_file)
        else GET_INT_OPTION("-f", "--frames-to-save", frames_to_save)
        else GET_INT_OPTION("-m", "--rotation-mode", rotation_mode)
        else GET_FLOAT_OPTION("-s", "--frames-per-second", frames_per_second)
        else GET_STRING_OPTION("-c", "--fourcc-code", fourcc_code)
        else GET_FLOAT_OPTION("-z", "--zoom", rescaling)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

void printhelp(void)
{
//------------------------------------------------------------------------------------------v
    printf("Usage: animator [OPTIONS]...\n");
    printf("Animates (rotates) 3D nifti images and saves video to a file.\n");
    printf("2016.08.30 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -w, --frame-width        positive integer number\n");
    printf("  -h, --frame-height       positive integer number\n");
    printf("  -r, --red-image          3D image for red channel\n");
    printf("  -g, --green-image        3D image for green channel\n");
    printf("  -b, --blue-image         3D image for blue channel\n");
    printf("  -R, --red-weight         red weight for alpha channel\n");
    printf("  -G, --green-weight       green weight for alpha channel\n");
    printf("  -B, --blue-weight        blue weight for alpha channel\n");
    printf("  -m, --rotation-mode      0, 1, ...3\n");
    printf("  -v, --video-file         output video file name\n");
    printf("  -f, --frames-to-save     positive integer number\n");
    printf("  -s, --frames-per-second  positive floating point number\n");
    printf("  -c, --fourcc-code        four-characters codec code, default is 'mp4v'\n");
    printf("  -z, --zoom               scaling ratio, default is 400\n");
    printf("  /?, --help\n");
    printf("Example: animator -r scaf.nii -g cell.nii -b overlay.nii -B 1.0 -v video.mov -c mp4v -f 250 -s 25 -m 1\n");
    printf("         animator -b scaf.nii -g cell.nii -r overlay.nii -R 1.0 -v image%04d.bmp -f 100 -s 0 -m 3\n\n");

}

//=============================================================================

int main(int argc, char *argv[])
{

    int ret = scan_parameters(argc, argv);
    if(isprinthelp)
    {
        printhelp();
        return ret;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try animator --help for more information.\n");
        fflush(stdout);
        return ret;
    }

    if(fourcc_code != NULL)
    {
        if(strlen(fourcc_code) < 4)
        {
            fprintf(stderr, "Invalid FOURCC.\n");
            return -1;
        }
    }
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    if(frames_per_second < 0) return -2;
    return a.exec();
}
