/*
 * vesselknife - 3D Image Processing and Analysis
 *
 * Copyright 2015  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BYTEIMAGE_H
#define BYTEIMAGE_H

#define BI_DIMENSIONS 3

#include <stdint.h>
#include <string>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <string.h>

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef short int16;

class Byte3DImage
{
public:
    Byte3DImage()
    {
        filePathAndName = NULL;
        info = NULL;
        data = NULL;
        framesInFile = 0;
        currentFrame = 0;
        for(int i = 0; i < BI_DIMENSIONS; i++)
        {
            size[i] = 0;
            voxelsize[i] = 1.0;
        }
    }
    ~Byte3DImage()
    {
        if(filePathAndName != NULL) delete[] filePathAndName;
        if(info != NULL) delete[] info;
        if(data != NULL) delete[] data;
    }

    unsigned char* pixel(unsigned int x[BI_DIMENSIONS])
    {
        unsigned int position = x[BI_DIMENSIONS - 1];
        for(int i = BI_DIMENSIONS - 2; i >= 0; i--)
        {
            position = position * size[i] + x[i];
        }
        return data + position;
    }

    unsigned char* pixel(const unsigned int x[BI_DIMENSIONS], const int dx[BI_DIMENSIONS], int* shift, int* steps)
    {
        int i;
        *steps = size[0]/2;
        if(*steps > size[1]/2) *steps = size[1]/2;
        if(*steps > size[2]/2) *steps = size[2]/2;
        if(dx[BI_DIMENSIONS - 1] > 0) *steps = size[BI_DIMENSIONS - 1] - x[BI_DIMENSIONS - 1] - 1;
        else if(dx[BI_DIMENSIONS - 1] < 0) *steps = x[BI_DIMENSIONS - 1];

        *shift = dx[BI_DIMENSIONS - 1];
        unsigned int position = x[BI_DIMENSIONS - 1];
        for(i = BI_DIMENSIONS - 2; i >= 0; i--)
        {
            *shift = *shift * (int)size[i] + dx[i];
            position = position * size[i] + x[i];

            if(dx[i] > 0)
            {
                if(*steps > size[i] - x[i] - 1) *steps = size[i] - x[i] - 1;
            }
            else if(dx[i] < 0)
            {
                if(*steps > x[i]) *steps = x[i];
            }
        }
        return data + position;
    }

    void allocate(unsigned int x[BI_DIMENSIONS])
    {
        if(data != NULL) delete[] data;
        for(int i = 0; i < BI_DIMENSIONS; i++)
        {
            size[i] = x[i];
            voxelsize[i] = 1.0;
        }

        data = new unsigned char[allsize()];
        memset(data, 0,  allsize());
    }
    unsigned int allsize()
    {
        unsigned int allsize = 1;
        for(int i = 0; i < BI_DIMENSIONS; i++) allsize *= size[i];
        return allsize;
    }

    void clear(void)
    {
       if(data != NULL) delete[] data;
       data = NULL;
       //deleteondestruct = true;
       for(int i = 0; i < BI_DIMENSIONS; i++)
       {
           size[i] = 0;
           voxelsize[i] = 1.0;
       }
    }

    bool crop(unsigned int min[BI_DIMENSIONS], unsigned int max[BI_DIMENSIONS])
    {
        unsigned int newsize[BI_DIMENSIONS];
        unsigned char* newdata;

        unsigned int allsize = 1;
        for(int i = 0; i < BI_DIMENSIONS; i++)
        {
            newsize[i] = max[i] - min[i] + 1;
            allsize *= newsize[i];
        }
        newdata = new unsigned char[allsize];

        unsigned int j;
        unsigned char* yy = newdata;
        unsigned int x[BI_DIMENSIONS];
        for(j = 0; j < BI_DIMENSIONS; j++) x[j] = min[j];
        do
        {
            unsigned char* xx = pixel(x);
            unsigned char* xxm = xx + max[0] - min[0];
            for(; xx <= xxm; ++xx, ++yy)
            {
                *yy = *xx;
            }
            for(j = 1; j < BI_DIMENSIONS; j++)
            {
                x[j]++;
                if(x[j] <= max[j])
                {
                    break;
                }
                else
                {
                    x[j] = min[j];
                }
            }
        }while(j < BI_DIMENSIONS);
        for(int i = 0; i < BI_DIMENSIONS; i++)
        {
            size[i] = newsize[i];
        }
        delete[] data;
        data = newdata;
        return true;
    }

    void zeromem(void)
    {
        memset(data, 0,  allsize());
    }
    char* info;
    unsigned char* data;
    unsigned int size[BI_DIMENSIONS];
    float voxelsize[BI_DIMENSIONS];
    char* filePathAndName;
    unsigned int framesInFile;
    unsigned int currentFrame;
};


#endif // BYTEIMAGE_H
