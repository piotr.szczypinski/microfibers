/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2016  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGEGL_H
#define IMAGEGL_H

#include <QWidget>
#include <QtOpenGL/QGLWidget>
#include <QtOpenGL/QGLFunctions>
#include <QVector3D>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QApplication>
#include "byteimage.h"

const static double sina = 3.48994967025009716459951816253e-2;
const static double cosa = 0.99939082701909573000624344004393;

#define CHANNEL_TYPE GL_UNSIGNED_BYTE
#define max_brightness_value 255
typedef GLubyte ChannelType;

//#define CHANNEL_TYPE GL_FLOAT
//#define max_brightness_value 1.0
//typedef GLfloat ChannelType;

class ImageGl : public QGLWidget, protected QGLFunctions
{
    Q_OBJECT

public:
    ImageGl(QWidget *parent, unsigned int pwidth, unsigned int pheight);

    void rotateView(double z, double y, double x);
    void setImage(unsigned int channel, Byte3DImage* rimage);
    Byte3DImage* getImage(unsigned int channel);
    void ReadPixels(unsigned int pwidth, unsigned int pheight, GLubyte* buffer);

    void setVolumeView(bool view);
    void setCrossView(int direction, bool view);

    void setBackgroundColor(unsigned int background);
    unsigned int backgroundColor(void) {return backgroundcolor;}
    void setZoom(double z);
    void setZoom(int z);
    void setStereo(int s, int d);
    void setCrossSection(int direction, unsigned int index);
    void setCrossSection(int direction, bool show);
    void setImageGrayWindowLow(double iminwindow);
    void setImageGrayWindowHigh(double imaxwindow);
    void getMatrix(double m[]);
    void setMatrix(double m[]);
    void setFreeze(bool f) {freeze = f;}
    void paintGL();

signals:
    void renderingFinished();

private:
    unsigned int pwidth;
    unsigned int pheight;
    //bool coarse;
    double minwindow;
    double maxwindow;
    unsigned int backgroundcolor;
    int zoomlevel;
    unsigned int sliceindex[3];
    bool renderslice[3];
    bool rendervolume;
    int volumelist;
    int stereo;
    int stereodepth;
    bool freeze;

    int lastdirectionofvolume[4];
    int mousex, mousey;
    void initializeGL();
    void paintGLSetMatrices(GLdouble modelview[16] = NULL, GLdouble projection[16] = NULL, GLint viewport[4] = NULL);
    Byte3DImage* image[4];
    GLdouble modelViewMat[16];
    void setIdentityMatrix(GLdouble W[16]);
    void copy4to4(GLdouble W[14], GLdouble W1[14]);
    void shift(int qx, int qy);
    void rotate(bool d, int l1, int l2);
    void rotatem(double rot, int l1, int l2, GLdouble W[14]);
    GLdouble determinant(GLdouble W[14]);
    GLdouble subdet3of4(GLdouble W[16], int w, int k);
    GLdouble invert3(GLdouble W[16], GLdouble W1[16]);
    int invert4(GLdouble W[16], GLdouble W1[16]);
    void multiply3x3(GLdouble W[16], GLdouble W1[16], GLdouble W2[16]);
    void multiply4x4(GLdouble W[16], GLdouble W1[16], GLdouble W2[16]);

    void getFacingDirection(int direction[3]);

    bool createVolumetricView(int direction);
    //bool createVolumetricView(int direction[3]);
    void renderCrossSection(unsigned int direction, unsigned int sliceindex, ChannelType *bits, unsigned int sizeimg[2], unsigned int sizetex[2]);
    bool fillCrossSection(ChannelType *bits, unsigned int stride, unsigned int direction, unsigned int sliceindex, bool noalpha = false, float aalpha = 1.0);
    bool createCrossSection(unsigned int direction, unsigned int sliceindex);
    void paintSingleGL(void);
    void setFalseColor(float level);
};

#endif // IMAGEGL_H
