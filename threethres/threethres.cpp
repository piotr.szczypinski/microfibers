/*
 * threethres
 * Optical microscopy of fiber scaffolds and bacteria image preprocessing.
 * Intended for segmentation of two channel data. One channel is for cell
 * the other for fiber scaffold. Program requires three threshold values.
 * Requires Insight Toolkit (ITK) version 4.20 or higher.
 *
 * Copyright (c) 2016 by Piotr M. Szczypiński
 * and NIST (National Institute of Standards and Technology)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of the NIST shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization from the NIST.
 */

#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
//#include <itkImageDuplicator.h>
#include <itkNeighborhoodIterator.h>


#include "getoption.h"

char* scaffold_image = (char*)"scaffold.nii";
char* cell_image = (char*)"cell.nii";
char* output_data = (char*)"output.nii";
float cell_background_threshold = 128;
float scaffold_background_threshold = 128;
float cell_scaffold_threshold = -1;

float cell_add = 0;
float scaffold_add = 0;

bool attachments = false;
bool isprinthelp = false;
bool minmax = false;

typedef itk::Image<float, 3>  ImageType;
typedef itk::Image<unsigned char, 3>  ByteImageType;
typedef itk::ImageFileReader<ImageType> ReaderType;
typedef itk::ImageFileWriter<ByteImageType> WriterType;

#define background_value 0
#define cell_value 127
#define scaffold_value 63
#define attachment_value 255



//=============================================================================

int scan_parameters(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-c", "--cell", cell_image)
        else GET_STRING_OPTION("-s", "--scaffold", scaffold_image)
        else GET_STRING_OPTION("-o", "--output", output_data)
        else GET_FLOAT_OPTION("-a", "--cell-scaffold", cell_scaffold_threshold)
        else GET_FLOAT_OPTION("-i", "--cell-backgrnd", cell_background_threshold)
        else GET_FLOAT_OPTION("-j", "--scaffold-backgrnd", scaffold_background_threshold)
        else GET_FLOAT_OPTION("-k", "--cell-bias", cell_add)
        else GET_FLOAT_OPTION("-l", "--scaffold-bias", scaffold_add)
        else GET_NOARG_OPTION("-n", "--attachments-only", attachments, true)
        else GET_NOARG_OPTION("-m", "--min-max", minmax, true)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

void printhelp(void)
{
//------------------------------------------------------------------------------------------v
    printf("Usage: threethres [OPTIONS]...\n");
    printf("For segmentation of two channel 3D image data.\n");
    printf("2016.08.17 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -c, --cell <file>           Load 3D cell image from <file>,\n");
    printf("  -s, --scaffold <file>       Load 3D scaffold image from <file>,\n");
    printf("  -o, --output <file>         Save results to <file>,\n");
    printf("  -a, --cell-scaffold <f>     Cell-scaffold threshold of 0-90 degrees,\n");
    printf("  -i, --cell-backgrnd <f>     Cell-background gray-scale threshold,\n");
    printf("  -j, --scaffold-backgrnd <f> Scaffold-background gray-scale threshold,\n");
    printf("  -k, --cell-bias <f>         Cell bias,\n");
    printf("  -l, --scaffold-bias <f>     Scaffold bias,\n");
    printf("  -n, --attachments-only    Remove scaffold and cell markings,\n");
    printf("  -m, --min-max    Remove scaffold and cell markings,\n");
    printf("  /?, --help                  Display this help and exit.\n\n");
}



//=============================================================================
// Creates Byte per pixel image of size mathcing the input image
void createImage(ImageType::Pointer input, ByteImageType::Pointer output)
{
    output->SetRegions(input->GetLargestPossibleRegion());
    output->SetDirection(input->GetDirection());
    output->SetSpacing(input->GetSpacing());
    output->Allocate();

    itk::ImageRegionIterator<ByteImageType> outputIterator(output, output->GetLargestPossibleRegion());

    while(!outputIterator.IsAtEnd())
    {
        outputIterator.Set(background_value);
        ++outputIterator;
    }

}

//=============================================================================
// Performs segmentation into three regions: cell, scaffold and background
void minMaxImage(ImageType::Pointer inputc, ImageType::Pointer inputs)
{
    itk::ImageRegionIterator<ImageType> cellIterator(inputc, inputc->GetLargestPossibleRegion());
    itk::ImageRegionIterator<ImageType> scafIterator(inputs, inputs->GetLargestPossibleRegion());

    float minc, maxc, mins, maxs;

    bool first = true;
    while(!cellIterator.IsAtEnd() && !scafIterator.IsAtEnd())
    {
        float c = cellIterator.Get()+cell_add;
        float s = scafIterator.Get()+scaffold_add;
        if(first)
        {
           minc = maxc = c;
           mins = maxs = s;
           first = false;
        }
        else
        {
            if(minc > c) minc = c;
            if(maxc < c) maxc = c;
            if(mins > s) mins = s;
            if(maxs < s) maxs = s;
        }
        ++cellIterator;
        ++scafIterator;
    }
    printf("        Minmax Cell: %f %f, Scaffold: %f %f\n", minc, maxc, mins, maxs);
    printf("        Thresholds Cell: %f, Scaffold: %f\n", cell_background_threshold, scaffold_background_threshold);
}

//=============================================================================
// Performs segmentation into three regions: cell, scaffold and background
void segmentImage(ImageType::Pointer inputc, ImageType::Pointer inputs, ByteImageType::Pointer output)
{
    itk::ImageRegionIterator<ImageType> cellIterator(inputc, inputc->GetLargestPossibleRegion());
    itk::ImageRegionIterator<ImageType> scafIterator(inputs, inputs->GetLargestPossibleRegion());
    itk::ImageRegionIterator<ByteImageType> outputIterator(output, output->GetLargestPossibleRegion());

    while(!cellIterator.IsAtEnd() && !scafIterator.IsAtEnd())
    {

        float c = cellIterator.Get()+cell_add;
        float s = scafIterator.Get()+scaffold_add;

        unsigned char o;
        if(c < cell_background_threshold && s < scaffold_background_threshold)
        {
            o = 0;
        }
        else
        {
            if(atan2(c, s) > cell_scaffold_threshold) o = cell_value;
            else o = scaffold_value;
        }
        outputIterator.Set(o);
        ++cellIterator;
        ++scafIterator;
        ++outputIterator;
    }
}
//=============================================================================
// Indicates voxels belonging to cell adjacent to voxels belonging to scaffold
void findContacts(ByteImageType::Pointer image)
{
    typedef itk::NeighborhoodIterator<ByteImageType> NeighborhoodIterator;
    typedef itk::ImageRegionIterator<ByteImageType> ImageIterator;

    NeighborhoodIterator::RadiusType radius;
    for (unsigned int i = 0; i < ByteImageType::ImageDimension; ++i) radius[i] = 1;
    NeighborhoodIterator it(radius, image, image->GetRequestedRegion());
    ImageIterator iit(image, image->GetRequestedRegion());

    for (it.Begin(); !it.IsAtEnd(); ++it, ++iit)
    {
        if(iit.Get() == cell_value)
        {
            for (unsigned int i = 0; i < it.Size(); ++i)
            {
                if(it.GetPixel(i) == scaffold_value)
                {
                    iit.Set(attachment_value);
                    break;
                }
            }
        }
    }
}

//=============================================================================
// Marks cell and scaffold voxels as background, preserves attachment marks only
void contactsOnly(ByteImageType::Pointer image)
{
    itk::ImageRegionIterator<ByteImageType> imageIterator(image, image->GetLargestPossibleRegion());
    while(!imageIterator.IsAtEnd())
    {
        if(imageIterator.Get() != attachment_value) imageIterator.Set(background_value);
        ++imageIterator;
    }
}

//=============================================================================
// Program starts in main()
int main(int argc, char *argv[])
{
    ImageType::Pointer inputs;
    ImageType::Pointer inputc;
    ReaderType::Pointer readers = ReaderType::New();
    ReaderType::Pointer readerc = ReaderType::New();

    int ret = scan_parameters(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp();
        return ret;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try 'threethres --help' for more information.\n");
        fflush(stdout);
        return ret;
    }

    if(cell_scaffold_threshold < 0 || cell_scaffold_threshold > 90)
    {
        //Determine cell_scaffold_threshold if not set
        cell_scaffold_threshold = atan2(cell_background_threshold, scaffold_background_threshold);
    }
    else
    {
        // Convert cell_scaffold_threshold from degrees to redians
        cell_scaffold_threshold = cell_scaffold_threshold/57.2957795131;
    }

    readers->SetFileName(scaffold_image);
    readers->Update();
    inputs = readers->GetOutput();
    if(inputs.IsNull())
    {
        printf("Cannot load from %s\n", scaffold_image); fflush(stdout);
        return -1;
    }
    readerc->SetFileName(cell_image);
    readerc->Update();
    inputc = readerc->GetOutput();
    if(inputc.IsNull())
    {
        printf("Cannot load from %s\n", cell_image); fflush(stdout);
        return -2;
    }

    bool match = true;
    printf("Cell image size: ");
    for (unsigned int i = 0; i < ImageType::ImageDimension; ++i)
    {
        int c1 = (int)inputc->GetRequestedRegion().GetSize()[i];
        int c2 = (int)inputs->GetRequestedRegion().GetSize()[i];
        printf("%i ", c1);
        if(c1 <= 0 || c1 != c2) match = false;
    }
    printf("\n");
    printf("Scaffold image size: ");
    for (unsigned int i = 0; i < ImageType::ImageDimension; ++i)
    {
        int c2 = (int)inputs->GetRequestedRegion().GetSize()[i];
        printf("%i ", c2);
    }
    printf("\n");
    if(!match)
    {
        printf("The image sizes do not match or image is empty.\n");
        return -3;
    }

    printf("------ Segmentation starts\n");
    fflush(stdout);
    ByteImageType::Pointer output = ByteImageType::New();
    createImage(inputc, output);

    if(minmax) minMaxImage(inputc, inputs);
    segmentImage(inputc, inputs, output);
    findContacts(output);
    if(attachments) contactsOnly(output);

    WriterType::Pointer writer = WriterType::New();
    writer->SetFileName(output_data);
    writer->SetInput(output);
    writer->Update();
    printf("------ Completed and saved to %s\n", output_data); fflush(stdout);

    return EXIT_SUCCESS;
}


